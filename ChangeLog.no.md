# Changelog
Alle viktige endringar til dette prosjektet vil bli dokumentert i denne fila.

Formatet er basert på [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
og prosjektet følg [semantiske versjonar](https://semver.org/spec/v2.0.0.html).

Dette er den norske endringsloggen, som skal speile den engelske frå
ChangeLog.md.

## [0.5.0] - 2021-03-16

### Lagt til
- Eit hjelp-vindauge som forklarar dei forskjellege styrkane

### Endra
- Ikonet er no eit «android adaptivt» ikon, dette vil løyse problem med korleis somme heimskjermar viser ikonet

## [0.4.0] - 2021-03-08

### Endra
- Du kan no halde nede på ein dato i kalenderen for å redigere eller leggje til ein oppføring på den datoen
- Vis namnet til eksterne omsetjarar i «om»-vindauget
- La til omsetjing der det mangla
- Tabelloverskriftar i eksporterte filer vil gjenta på kvar side viss ein tabell går over meir enn ei enkelt side
- Oppreinsking av koden

## [0.3.1] - 2021-03-06

### Lagt til
- Finsk omsetjing av Mika Latvala
- Metadata som er naudsynt for f-droid

## [0.3.0] - 2021-03-02

### Lagt til
- Støtte for å skjule og vise individuelle månadar i den eksporterte fila
- Støtte for å sortere tabellane i den eksporterte fila
- Støtte for å avgrense kor mange månader som vart vist i den eksporterte fila

### Fiksa
- Meldinga som kjem etter importering er no òg på Norsk

## [0.2.0] - 2021-02-27

### Lagt til
- Støtte for å importere data frå eksporterte filer

### Endra
- Eksportert data er no inndelt etter månad
- Eksportert data har no oppsummeringar, sånn som dei som er i statistikk-fana

### Fiksa
- Tooltips for fanelinja er no òg på Norsk

## [0.1.2] - 2021-02-24

### Fiksa
- Fiksa «hjelp»-vindauget

## [0.1.1] - 2021-02-23

### Lagt til
- La til lenkjer i «om»-vindauget

### Fiksa
- Fiksa høvet til å omsetje teksten for den valde månaden i kalenderen og
    statistikk-fana

## [0.1.0] - 2021-02-23
- Fyrste utgiving
