# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.5.0] - 2021-03-16

### Added
- A help dialog that explains the various strengths

### Changed
- The icon is now provided as an android adaptive icon, this should resolve issues with the icon in certain launchers

## [0.4.0] - 2021-03-08

### Changed
- Long pressing a date in the calendar will now add or edit an entry on that date
- Credit translators in the about dialog
- Various translation updates
- Table headers in exported files will now repeat when printing if a table spans more than one page
- Code cleanup

## [0.3.1] - 2021-03-06

### Added
- Finnish translation by Mika Latvala
- Metadata required for f-droid

## [0.3.0] - 2021-03-02

### Added
- Support for hiding and showing individual months in the exported file
- Support for sorting the tables in the exported file
- Support for limiting the number of months displayed in the exported file

### Fixed
- The import success message can now be translated

## [0.2.0] - 2021-02-27

### Added
- Support for importing data from exported files

### Changed
- Exported data is now section into monthly tables
- Exported data now contains summary lines, like those found in the stats tab

### Fixed
- Tooltips for entries in the tab bar now translatable

## [0.1.2] - 2021-02-24

### Fixed
- Fixed the "help"-window

## [0.1.1] - 2021-02-23

### Added
- Added links to the about dialog

### Fixed
- Fixed the ability to translate the screen reader text for the current month

## [0.1.0] - 2021-02-23
- Initial release
