{
  "@@last_modified": "2021-03-14T16:22:36.864015",
  "General": "General",
  "@General": {
    "description": "Used as a header in the config screen to refer to \"general settings\"",
    "type": "text",
    "placeholders": {}
  },
  "Continue": "Continue",
  "@Continue": {
    "description": "Used as a button in the 'first time' configuration screen",
    "type": "text",
    "placeholders": {}
  },
  "Use neutral language": "Use neutral language",
  "@Use neutral language": {
    "description": "Toggle button that enables (or disables) use of \"headache\" instead of \"migraine\"",
    "type": "text",
    "placeholders": {}
  },
  "Use the term \"headache\" rather than \"migraine\"": "Use the term \"headache\" rather than \"migraine\"",
  "@Use the term \"headache\" rather than \"migraine\"": {
    "type": "text",
    "placeholders": {}
  },
  "What kind of headache do you have?": "What kind of headache do you have?",
  "@What kind of headache do you have?": {
    "type": "text",
    "placeholders": {}
  },
  "Select this if you're unsure": "Select this if you're unsure",
  "@Select this if you're unsure": {
    "type": "text",
    "placeholders": {}
  },
  "Migraine": "Migraine",
  "@Migraine": {
    "description": "Used as level 2/3 (medium) headache strength",
    "type": "text",
    "placeholders": {}
  },
  "Other (general headaches, cluster etc.)": "Other (general headaches, cluster etc.)",
  "@Other (general headaches, cluster etc.)": {
    "type": "text",
    "placeholders": {}
  },
  "Settings": "Settings",
  "@Settings": {
    "type": "text",
    "placeholders": {}
  },
  "Configuration": "Configuration",
  "@Configuration": {
    "description": "The name of the first time configuration screen. This is pretty much like the settings screen, but with some different information and displayed only once, therefore using a different name to make this clearer.",
    "type": "text",
    "placeholders": {}
  },
  "Medication list": "Medication list",
  "@Medication list": {
    "description": "Header for the part of the configuration screen where the user sets up their medication",
    "type": "text",
    "placeholders": {}
  },
  "Changing the list of medications here will not change medications in entries you have already added.": "Changing the list of medications here will not change medications in entries you have already added.",
  "@Changing the list of medications here will not change medications in entries you have already added.": {
    "type": "text",
    "placeholders": {}
  },
  "If you do not wish to add your medications, just press \"Continue\" and Migraine Log will add two generic ones for you to use.": "If you do not wish to add your medications, just press \"Continue\" and Migraine Log will add two generic ones for you to use.",
  "@If you do not wish to add your medications, just press \"Continue\" and Migraine Log will add two generic ones for you to use.": {
    "type": "text",
    "placeholders": {}
  },
  "Add a new medication": "Add a new medication",
  "@Add a new medication": {
    "type": "text",
    "placeholders": {}
  },
  "Undo": "Undo",
  "@Undo": {
    "type": "text",
    "placeholders": {}
  },
  "_deletedElement": "Deleted \"{medication}\"",
  "@_deletedElement": {
    "description": "A pop-up message after the user has deleted a medication. Is accompanied with an 'undo' button.",
    "type": "text",
    "placeholders": {
      "medication": {}
    }
  },
  "_andMessage": "{med1} and {med2}",
  "@_andMessage": {
    "description": "Used to join two (or more) medications together. med1 is either a single medication, or a comma-separated list of medications. med2 is always just a single medication. Neither can be empty/null.",
    "type": "text",
    "placeholders": {
      "med1": {},
      "med2": {}
    }
  },
  "No registration": "No registration",
  "@No registration": {
    "description": "Used in the pie diagram and table on the front page to denote days where no entries have been added. Avoid terminology like 'no headaches', since that might not be quite correct.",
    "type": "text",
    "placeholders": {}
  },
  "Headache": "Headache",
  "@Headache": {
    "description": "Used as level 1/3 (lowest) headache strength",
    "type": "text",
    "placeholders": {}
  },
  "Mild headache": "Mild headache",
  "@Mild headache": {
    "description": "Used as neutral level 1/3 (lowest) headache strength",
    "type": "text",
    "placeholders": {}
  },
  "Moderate headache": "Moderate headache",
  "@Moderate headache": {
    "description": "Used as neutral level 2/3 (medium) headache strength",
    "type": "text",
    "placeholders": {}
  },
  "Strong migraine": "Strong migraine",
  "@Strong migraine": {
    "description": "Used as level 3/3 (strongest) headache strength",
    "type": "text",
    "placeholders": {}
  },
  "Strong headache": "Strong headache",
  "@Strong headache": {
    "description": "Used as neutral level 3/3 (strongest) headache strength",
    "type": "text",
    "placeholders": {}
  },
  "Migraine medication": "Migraine medication",
  "@Migraine medication": {
    "description": "Used as a generic 'medication' when the user opts not to add their own",
    "type": "text",
    "placeholders": {}
  },
  "Seizure medication": "Seizure medication",
  "@Seizure medication": {
    "description": "Used as neutral generic 'medication' when the user opts not to add their own",
    "type": "text",
    "placeholders": {}
  },
  "Painkillers": "Painkillers",
  "@Painkillers": {
    "description": "Used as the second 'medication' when the user opts not to add their own",
    "type": "text",
    "placeholders": {}
  },
  "Edit": "Edit",
  "@Edit": {
    "type": "text",
    "placeholders": {}
  },
  "Register": "Register",
  "@Register": {
    "description": "Displayed as the header when registering a new migraine attack",
    "type": "text",
    "placeholders": {}
  },
  "You must select a strength": "You must select a strength",
  "@You must select a strength": {
    "description": "Notice that appears if the user tries to save an entry without selecting a strength",
    "type": "text",
    "placeholders": {}
  },
  "Save": "Save",
  "@Save": {
    "type": "text",
    "placeholders": {}
  },
  "editingEntryNewDate": "You're editing an entry on {editDate}. If you change the date then this entry will be moved to the new date.",
  "@editingEntryNewDate": {
    "type": "text",
    "placeholders": {
      "editDate": {}
    }
  },
  "Warning: there's already an entry on this date. If you save then the existing entry will be overwritten.": "Warning: there's already an entry on this date. If you save then the existing entry will be overwritten.",
  "@Warning: there's already an entry on this date. If you save then the existing entry will be overwritten.": {
    "type": "text",
    "placeholders": {}
  },
  "Date": "Date",
  "@Date": {
    "type": "text",
    "placeholders": {}
  },
  "change date": "change date",
  "@change date": {
    "description": "Used as an action label on the button that triggers a calendar to select the date for an attack when a screen reader is used. It will be contextualized by the OS, on Android the OS will say the label of the button, then the word 'button', followed by 'double tap to change date'",
    "type": "text",
    "placeholders": {}
  },
  "Strength": "Strength",
  "@Strength": {
    "type": "text",
    "placeholders": {}
  },
  "Pain is very subjective. The only person that knows which option is right, is you. If you're uncertain, select the higher one of the two you're considering.": "Pain is very subjective. The only person that knows which option is right, is you. If you're uncertain, select the higher one of the two you're considering.",
  "@Pain is very subjective. The only person that knows which option is right, is you. If you're uncertain, select the higher one of the two you're considering.": {
    "type": "text",
    "placeholders": {}
  },
  "Unable to perform most activities": "Unable to perform most activities",
  "@Unable to perform most activities": {
    "description": "Migraine strength 3/3",
    "type": "text",
    "placeholders": {}
  },
  "Unable to perform some activities": "Unable to perform some activities",
  "@Unable to perform some activities": {
    "description": "Migraine strength 2/3",
    "type": "text",
    "placeholders": {}
  },
  "Able to perform most activities": "Able to perform most activities",
  "@Able to perform most activities": {
    "description": "Migraine strength 1/3",
    "type": "text",
    "placeholders": {}
  },
  "Close": "Close",
  "@Close": {
    "description": "Used as a 'close' button in the help dialog for migraine strength. This will be converted to upper case when used in this context, ie. 'Close' becomes 'CLOSE'. This to fit with Android UI guidelines",
    "type": "text",
    "placeholders": {}
  },
  "Which strength should I choose?": "Which strength should I choose?",
  "@Which strength should I choose?": {
    "description": "Used as the tooltip for the help button in the 'add' and 'edit' screens as well as the title of the help window that pops up when this button is pressed.",
    "type": "text",
    "placeholders": {}
  },
  "Medications taken": "Medications taken",
  "@Medications taken": {
    "description": "Header in the add/edit window",
    "type": "text",
    "placeholders": {}
  },
  "Note": "Note",
  "@Note": {
    "type": "text",
    "placeholders": {}
  },
  "(none)": "(none)",
  "@(none)": {
    "description": "Used in exported HTML when the user has taken no medications. It is an entry in a table where the header is 'Medications'",
    "type": "text",
    "placeholders": {}
  },
  "Medications": "Medications",
  "@Medications": {
    "description": "Used as a table header in exported data",
    "type": "text",
    "placeholders": {}
  },
  "Migraine Log": "Migraine Log",
  "@Migraine Log": {
    "type": "text",
    "placeholders": {}
  },
  "hide": "hide",
  "@hide": {
    "description": "Used in exported HTML as a link that hides one month. Will be displayed like '[hide]', so it should be lower case unless there are good reasons not to",
    "type": "text",
    "placeholders": {}
  },
  "Hide this month": "Hide this month",
  "@Hide this month": {
    "description": "Tooltip for a link that hides a month from view in the exported HTML",
    "type": "text",
    "placeholders": {}
  },
  "hidden:": "hidden:",
  "@hidden:": {
    "description": "Will be rendered like: \"hidden: january 2021\"",
    "type": "text",
    "placeholders": {}
  },
  "Total headache days": "Total headache days",
  "@Total headache days": {
    "description": "Used in a table, will render like 'Total headache days    20 days'",
    "type": "text",
    "placeholders": {}
  },
  "Taken medication": "Taken medication",
  "@Taken medication": {
    "description": "Used in a table, will render like 'Taken Medication    5 days'",
    "type": "text",
    "placeholders": {}
  },
  "Filter:": "Filter:",
  "@Filter:": {
    "description": "Will be joined with the string 'show' and a dropdown box, full context could be: 'Filter: show [everything]",
    "type": "text",
    "placeholders": {}
  },
  "show": "show",
  "@show": {
    "description": "Will be joined with the string 'Filter:' and a dropdown box, full context could be: 'Filter: show [everything]",
    "type": "text",
    "placeholders": {}
  },
  "everything": "everything",
  "@everything": {
    "description": "Will be joined with the string 'Filter: show' and a dropdown box, full context could be: 'Filter: show [everything]",
    "type": "text",
    "placeholders": {}
  },
  "months": "months",
  "@months": {
    "description": "Will be joined with a number, which will be 3 or higher, to become ie. '3 months'",
    "type": "text",
    "placeholders": {}
  },
  "_generatedMessage": "Generated by Migraine Log version {version}",
  "@_generatedMessage": {
    "type": "text",
    "placeholders": {
      "version": {}
    }
  },
  "Loading...": "Loading...",
  "@Loading...": {
    "description": "Used in exported HTML to indicate that we're loading the exported data",
    "type": "text",
    "placeholders": {}
  },
  "Previous month": "Previous month",
  "@Previous month": {
    "description": "Tooltip for the previous month button",
    "type": "text",
    "placeholders": {}
  },
  "Next month": "Next month",
  "@Next month": {
    "description": "Tooltip for the next month button",
    "type": "text",
    "placeholders": {}
  },
  "_currentMonth": "Current month: {month}",
  "@_currentMonth": {
    "description": "Used for screen readers to describe the element in the month selector that indicates the current month. The month variable will contain a pre-localized month name along with the year",
    "type": "text",
    "placeholders": {
      "month": {}
    }
  },
  "Migraine Log helps you maintain a headache diary. Once you have added an entry, this screen will be replaced with statistics.\n\nAdd a new entry by pressing the \"+\"-button.\n\nFor more help, select \"Help\" in the menu at the top right of the screen.": "Migraine Log helps you maintain a headache diary. Once you have added an entry, this screen will be replaced with statistics.\n\nAdd a new entry by pressing the \"+\"-button.\n\nFor more help, select \"Help\" in the menu at the top right of the screen.",
  "@Migraine Log helps you maintain a headache diary. Once you have added an entry, this screen will be replaced with statistics.\n\nAdd a new entry by pressing the \"+\"-button.\n\nFor more help, select \"Help\" in the menu at the top right of the screen.": {
    "type": "text",
    "placeholders": {}
  },
  "Help": "Help",
  "@Help": {
    "type": "text",
    "placeholders": {}
  },
  "Icons in the calendar": "Icons in the calendar",
  "@Icons in the calendar": {
    "description": "Header in the help screen",
    "type": "text",
    "placeholders": {}
  },
  "Colours": "Colours",
  "@Colours": {
    "description": "Header in the help screen",
    "type": "text",
    "placeholders": {}
  },
  "An underline under a day in the calendar means that there is a registered entry on that date, but that no medication was taken.": "An underline under a day in the calendar means that there is a registered entry on that date, but that no medication was taken.",
  "@An underline under a day in the calendar means that there is a registered entry on that date, but that no medication was taken.": {
    "type": "text",
    "placeholders": {}
  },
  "A circle around a day in the calendar means that there is a registered entry on that date, and that medication was taken.": "A circle around a day in the calendar means that there is a registered entry on that date, and that medication was taken.",
  "@A circle around a day in the calendar means that there is a registered entry on that date, and that medication was taken.": {
    "type": "text",
    "placeholders": {}
  },
  "Througout Migraine Log, colours are used to indicate the strength of a migraine. The underline/circle in the calendar, as well as the text of the strength when adding or editing an entry, will use a colour to signify the strength.": "Througout Migraine Log, colours are used to indicate the strength of a migraine. The underline/circle in the calendar, as well as the text of the strength when adding or editing an entry, will use a colour to signify the strength.",
  "@Througout Migraine Log, colours are used to indicate the strength of a migraine. The underline/circle in the calendar, as well as the text of the strength when adding or editing an entry, will use a colour to signify the strength.": {
    "type": "text",
    "placeholders": {}
  },
  "daysPlural": "{days} days",
  "@daysPlural": {
    "type": "text",
    "placeholders": {
      "days": {}
    }
  },
  "daySignular": "{day} day",
  "@daySignular": {
    "type": "text",
    "placeholders": {
      "day": {}
    }
  },
  "Successfully imported data": "Successfully imported data",
  "@Successfully imported data": {
    "type": "text",
    "placeholders": {}
  },
  "The file is corrupt and can not be imported": "The file is corrupt and can not be imported",
  "@The file is corrupt and can not be imported": {
    "type": "text",
    "placeholders": {}
  },
  "This file does not appear to be a Migraine Log (html) file": "This file does not appear to be a Migraine Log (html) file",
  "@This file does not appear to be a Migraine Log (html) file": {
    "type": "text",
    "placeholders": {}
  },
  "This file is from a newer version of Migraine Log. Upgrade the app first.": "This file is from a newer version of Migraine Log. Upgrade the app first.",
  "@This file is from a newer version of Migraine Log. Upgrade the app first.": {
    "type": "text",
    "placeholders": {}
  },
  "An unknown error ocurred during import.": "An unknown error ocurred during import.",
  "@An unknown error ocurred during import.": {
    "type": "text",
    "placeholders": {}
  },
  "Import failed:": "Import failed:",
  "@Import failed:": {
    "description": "Will contain information about why after the :",
    "type": "text",
    "placeholders": {}
  },
  "Your Migraine Log version is too old to read the data you have. Please upgrade Migraine Log.": "Your Migraine Log version is too old to read the data you have. Please upgrade Migraine Log.",
  "@Your Migraine Log version is too old to read the data you have. Please upgrade Migraine Log.": {
    "type": "text",
    "placeholders": {}
  },
  "Migraine Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.\n\nThis program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.": "Migraine Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.\n\nThis program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.",
  "@Migraine Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.\n\nThis program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.": {
    "type": "text",
    "placeholders": {}
  },
  "Copyright ©": "Copyright ©",
  "@Copyright ©": {
    "description": "Will be followed by the author name and copyright year, which will be a link",
    "type": "text",
    "placeholders": {}
  },
  "See the": "See the",
  "@See the": {
    "description": "Used to construct the sentence 'See the GNU General Public License for details', where 'GNU General Public License' will be a link. Spacing around 'GNU General Public License' is automatic.",
    "type": "text",
    "placeholders": {}
  },
  "for details.": "for details.",
  "@for details.": {
    "description": "Used to construct the sentence 'See the GNU General Public License for details', where 'GNU General Public License' will be a link. Spacing around 'GNU General Public License' is automatic.",
    "type": "text",
    "placeholders": {}
  },
  "About": "About",
  "@About": {
    "description": "Menu entry for triggering the about dialog",
    "type": "text",
    "placeholders": {}
  },
  "Exit": "Exit",
  "@Exit": {
    "description": "Used to exit the app",
    "type": "text",
    "placeholders": {}
  },
  "Export": "Export",
  "@Export": {
    "type": "text",
    "placeholders": {}
  },
  "Import": "Import",
  "@Import": {
    "type": "text",
    "placeholders": {}
  },
  "Statistics": "Statistics",
  "@Statistics": {
    "description": "Tooltip for the statistics tab",
    "type": "text",
    "placeholders": {}
  },
  "Calendar": "Calendar",
  "@Calendar": {
    "description": "Tooltip for the calendar tab",
    "type": "text",
    "placeholders": {}
  },
  "Home": "Home",
  "@Home": {
    "description": "Tooltip for the home tab",
    "type": "text",
    "placeholders": {}
  },
  "Translated by": "Translated by",
  "@Translated by": {
    "description": "Should be a literal translation of this phrase, the value of TRANSLATED_BY will be appended to make a string like this: \"Translated by: TRANSLATED_BY\"",
    "type": "text",
    "placeholders": {}
  },
  "TRANSLATED_BY": "TRANSLATED_BY",
  "@TRANSLATED_BY": {
    "description": "Should be an alphabetical list of the names of the translators of this language, delimited with commas or the localized equivalent of \"and\". Will be displayed in the about dialog like this: \"Translated by: TRANSLATED_BY\"",
    "type": "text",
    "placeholders": {}
  },
  "deletedDateMessage": "Deleted {fmtDate}",
  "@deletedDateMessage": {
    "type": "text",
    "placeholders": {
      "fmtDate": {}
    }
  },
  "Delete": "Delete",
  "@Delete": {
    "type": "text",
    "placeholders": {}
  },
  "Add": "Add",
  "@Add": {
    "type": "text",
    "placeholders": {}
  },
  "Show action menu": "Show action menu",
  "@Show action menu": {
    "type": "text",
    "placeholders": {}
  },
  "lastNDaysMessage": "Last {days} days",
  "@lastNDaysMessage": {
    "description": "days should always be >1, so plural",
    "type": "text",
    "placeholders": {
      "days": {}
    }
  },
  "took no medication": "took no medication",
  "@took no medication": {
    "type": "text",
    "placeholders": {}
  },
  "tookMedsMessage": "took {meds}",
  "@tookMedsMessage": {
    "description": "This will expand into a list of medications taken, and will be added to the type of headache the user had. For instance, this might get 'Imigran' as meds, this will then be 'took Imigran', and it might expand into the sentece 'Migraine, took Imigran'",
    "type": "text",
    "placeholders": {
      "meds": {}
    }
  },
  "Note:": "Note:",
  "@Note:": {
    "type": "text",
    "placeholders": {}
  }
}