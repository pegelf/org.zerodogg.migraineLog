// Migraine Log - a simple multi-platform headache diary
// Copyright (C) 2021    Eskild Hustvedt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/services.dart'
    show SystemChrome, SystemUiOverlayStyle, SystemNavigator;
import 'package:intl/date_symbol_data_local.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:intl/intl.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:package_info/package_info.dart';
import 'dart:math';
import 'config.dart';
import "genericwidgets.dart";
import 'helpwelcome.dart';
import 'stats.dart';
import 'localehack.dart';
import 'definitions.dart';
import 'colors.dart';
import 'editor.dart';
import 'datatypes.dart';
import 'viewer.dart';
import 'exporter.dart';
import 'importer.dart';
import 'i18n/messages_all.dart';

/// Main entry point
void main() {
  /// Enable dark theme for the system
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);

  /// Initialize the app
  runApp(MigraineLog());
}

class MigraineLog extends StatefulWidget {
  MigraineLog({this.customInitialization});

  /// customInitialization is used by the screenshot integration tests to
  /// populate the app with a known data state before taking screenshots
  @visibleForTesting
  final Function(MigraineLogGlobalState, MigraineLogConfig, MigraineList)
      customInitialization;

  @override
  MigraineLogState createState() => MigraineLogState();
}

/// Our root widget
@visibleForTesting
class MigraineLogState extends State<MigraineLog> {
  // Our configuration
  MigraineLogConfig config = MigraineLogConfig();
  // The global state object
  MigraineLogGlobalState state = MigraineLogGlobalState();
  // Our root list. This gets initialized in initState()
  MigraineList list;

  @override
  // Initialize our MigraineList instance
  void initState() {
    super.initState();
    list = MigraineList(config: config);
    if (widget.customInitialization != null) {
      widget.customInitialization(state, config, list);
    }
  }

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MigraineLogAppLifecycle(
        list: list,
        config: config,
        child: MultiProvider(
          /// All subcomponents should have access to the list, the state and
          /// the config
          providers: [
            ChangeNotifierProvider.value(value: list),
            ChangeNotifierProvider.value(value: state),
            ChangeNotifierProvider.value(value: config),
          ],
          child: MaterialApp(
            title: 'MigraineLog',
            theme: ThemeData(
              primarySwatch: MDColors.primarySwatch,
              accentColor: MDColors.accentColor,
              // This makes the visual density adapt to the platform
              visualDensity: VisualDensity.adaptivePlatformDensity,
              // We're always dark
              brightness: Brightness.dark,
            ),

            /// Needed for flutter localization
            localizationsDelegates: [
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
            ],

            /// For the most part this isn't used, but left in to make flutter happy.
            /// For the actual locale logic, see localehack.dart
            supportedLocales: [
              const Locale('nn', ''),
              const Locale('nb', ''),
              const Locale('fi', ''),
              const Locale.fromSubtags(languageCode: 'en'),
            ],

            /// Use LocaleHack to detect the locale
            localeResolutionCallback: (_, __) => LocaleHack.getFlutterLocale(),

            /// Provide the MigraineList, MigraineLogConfig and
            /// MigraineLogGlobalState globally
            /// Our root widget is the loader wrapper
            home: Consumer<MigraineLogConfig>(
              builder: (context, config, _) => MigraineLogLoader(
                  title: "Migraine Log",
                  state: state,
                  list: list,
                  config: config),
            ),

            /// Routing
            routes: <String, WidgetBuilder>{
              /// The editor for MigraineEntry
              '/editor': (BuildContext context) {
                /// Retrieve parameters
                final MigraineEditorPathParameters params =
                    ModalRoute.of(context).settings.arguments;
                MigraineLogEditor editor;

                /// If we have no parameters, then create a new entry
                if (params == null) {
                  editor = MigraineLogEditor(
                    entry: list.newEntry(),
                  );
                }

                /// If we have an entry parameter, then assume that we're suppose to edit that entry
                else if (params.entry != null) {
                  editor = MigraineLogEditor(
                    editMode: params.entry.date,

                    /// We clone the entry to allow the user to cancel editing
                    entry: params.entry.clone(),
                  );
                }

                /// If we don't have an entry parameter, then create a new entry on the provided date
                else {
                  editor = MigraineLogEditor(
                    entry: list.newEntry(date: params.date),
                  );
                }
                return editor;
              },

              '/config': (BuildContext context) => MigraineLogConfigScreen(),

              /// The help screen
              '/help': (BuildContext context) => MigraineLogHelp(),
            },
          ),
        ));
  }
}

/// Wrapper that loads resources and then renders MigraineLogHome
class MigraineLogLoader extends StatefulWidget {
  MigraineLogLoader(
      {Key key,
      @required this.title,
      @required this.list,
      @required this.config,
      @required this.state})
      : super(key: key);

  final String title;
  final MigraineList list;
  final MigraineLogConfig config;
  final MigraineLogGlobalState state;

  @override
  _MigraineLogLoaderState createState() => _MigraineLogLoaderState();
}

class _MigraineLogLoaderState extends State<MigraineLogLoader> {
  /// Used to track how many components have initialized so far
  int initialized = 0;

  /// The number of components that we should initialize before displaying
  /// MigraineLogHome.
  final int totalComponentsToInitialize = 5;
  PackageInfo packageInfo;
  String _errorMessage;

  String _dataVersionErrorMessage() {
    return Intl.message(
        "Your Migraine Log version is too old to read the data you have. Please upgrade Migraine Log.");
  }

  @override
  void initState() {
    super.initState();

    /// Initialize the date locale for Intl
    initializeDateFormatting(LocaleHack.dateLocale, null).then((_) {
      setState(() => {initialized++});
    });

    /// Loads previous Migraine Log data
    widget.list.loadData().then((result) {
      if (result == false) {
        // We failed, error out
        setState(() => {_errorMessage = _dataVersionErrorMessage()});
      } else {
        setState(() => {initialized++});
      }
    });

    widget.config.loadConfig().then((_) {
      setState(() => {initialized++});
    });

    /// Initializes Intl
    initializeMessages(LocaleHack.detectLocale()).then((_) {
      LocaleHack.intlInit();
      addLicenses();
      setState(() => {initialized++});
    });
    PackageInfo.fromPlatform().then((PackageInfo info) {
      packageInfo = info;
      widget.state.version = info.version;
      setState(() => {initialized++});
    });
  }

  @override
  Widget build(BuildContext context) {
    /// If everything has been initailized, render MigraineLogHome
    if (initialized >= totalComponentsToInitialize) {
      if (widget.config.onboardingVersion < 1) {
        return MigraineLogFirstTimeConfig();
      }
      return MigraineLogHome(title: widget.title, packageInfo: packageInfo);
    } else {
      Widget child;
      if (_errorMessage != null) {
        child = MDTextBox(text: _errorMessage);
      } else {
        child = SizedBox(
          child: CircularProgressIndicator(),
          height: 50,
          width: 50,
        );
      }

      /// We're still loading, so display a progress indicator instead
      return Scaffold(
        body: Center(
          child: child,
        ),
      );
    }
  }
}

void addLicenses() {
  LicenseRegistry.addLicense(() async* {
    yield LicenseEntryWithLineBreaks(<String>['Migraine Log'], '''
Copyright © Eskild Hustvedt 2021

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
''');
  });
}

/// The actual home screen
class MigraineLogHome extends StatefulWidget {
  MigraineLogHome({Key key, this.title, @required this.packageInfo})
      : super(key: key);

  final String title;
  final PackageInfo packageInfo;

  @override
  _MigraineLogHomeState createState() => _MigraineLogHomeState();
}

enum HomeMenuItem { exit, about, help, export, import, settings, debug }

class _MigraineLogHomeState extends State<MigraineLogHome>

    /// Needed for the vsync option for TabController
    with
        SingleTickerProviderStateMixin {
  @visibleForTesting
  TabController controller;

  String _legaleseMessage() {
    return Intl.message(
      "Migraine Log is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.\n\nThis program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.",
    );
  }

  String _copyrightMessage() {
    return Intl.message(
      "Copyright ©",
      desc:
          "Will be followed by the author name and copyright year, which will be a link",
    );
  }

  String _legaleseLinkPrefix() {
    return Intl.message("See the",
        desc:
            "Used to construct the sentence 'See the GNU General Public License for details', where 'GNU General Public License' will be a link. Spacing around 'GNU General Public License' is automatic.");
  }

  String _legaleseLinkPostfix() {
    return Intl.message("for details.",
        desc:
            "Used to construct the sentence 'See the GNU General Public License for details', where 'GNU General Public License' will be a link. Spacing around 'GNU General Public License' is automatic.");
  }

  String _aboutMessage() {
    return Intl.message('About',
        desc: "Menu entry for triggering the about dialog");
  }

  String _helpMessage() {
    return Intl.message('Help');
  }

  String _exitMessage() {
    return Intl.message('Exit', desc: "Used to exit the app");
  }

  String _appName() {
    return Intl.message("Migraine Log");
  }

  String _settingsMessage() {
    return Intl.message('Settings');
  }

  String _exportMessage() {
    return Intl.message('Export');
  }

  String _importMessage() {
    return Intl.message('Import');
  }

  String _statisticsMessage() {
    return Intl.message('Statistics', desc: "Tooltip for the statistics tab");
  }

  String _calendarMessage() {
    return Intl.message('Calendar', desc: "Tooltip for the calendar tab");
  }

  String _homeMessage() {
    return Intl.message("Home", desc: "Tooltip for the home tab");
  }

  String _translatedByPrefixMessage() {
    return Intl.message('Translated by',
        desc:
            'Should be a literal translation of this phrase, the value of TRANSLATED_BY will be appended to make a string like this: "Translated by: TRANSLATED_BY"');
  }

  String _translatorMessage() {
    return Intl.message('TRANSLATED_BY',
        desc:
            'Should be an alphabetical list of the names of the translators of this language, delimited with commas or the localized equivalent of "and". Will be displayed in the about dialog like this: "Translated by: TRANSLATED_BY"');
  }

  /// Initialize the TabController before rendering
  @override
  void initState() {
    super.initState();
    controller = TabController(vsync: this, length: 3);
    controller.addListener(() => setState(() => {}));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Material(
        /// The tab bar at the bottom of the screen
        child: TabBar(
          tabs: <Tab>[
            Tab(
              key: Key('home_tab'),
              icon: Tooltip(
                message: _homeMessage(),
                child: Icon(Icons.home),
              ),
            ),
            Tab(
                key: Key('calendar_tab'),
                icon: Tooltip(
                  message: _calendarMessage(),
                  child: Icon(Icons.calendar_today),
                )),
            Tab(
              icon: Tooltip(
                message: _statisticsMessage(),
                child: Icon(Icons.pie_chart),
              ),
            ),
          ],
          controller: controller,
        ),
        color: Theme.of(context).primaryColor,
      ),
      appBar: AppBar(
        title: Text(_appName()),
        actions: <Widget>[
          Builder(
            builder: (BuildContext context) => PopupMenuButton(
              onSelected: (HomeMenuItem result) {
                switch (result) {
                  case HomeMenuItem.about:
                    {
                      var translator = _translatorMessage();
                      showAboutDialog(
                          context: context,
                          applicationName: _appName(),
                          children: [
                            RichText(
                              text: TextSpan(
                                children: [
                                  TextSpan(children: [
                                    TextSpan(text: _copyrightMessage() + ' '),
                                    TextSpan(
                                      text: "Eskild Hustvedt",
                                      style:
                                          TextStyle(color: MDColors.linkColor),
                                      recognizer: TapGestureRecognizer()
                                        ..onTap = () async {
                                          await launch(WebsiteURL);
                                        },
                                    ),
                                    TextSpan(text: " 2021"),
                                    if (translator != 'TRANSLATED_BY' &&
                                        translator != 'Eskild Hustvedt')
                                      TextSpan(
                                          text: "\n" +
                                              _translatedByPrefixMessage() +
                                              ': ' +
                                              translator),
                                  ]),
                                  TextSpan(
                                      text: "\n\n" + _legaleseMessage() + ' '),
                                  TextSpan(
                                    children: [
                                      TextSpan(text: _legaleseLinkPrefix()),
                                      TextSpan(
                                        text: " GNU General Public License ",
                                        style: TextStyle(
                                            color: MDColors.linkColor),
                                        recognizer: TapGestureRecognizer()
                                          ..onTap = () async {
                                            await launch(GPL_URL);
                                          },
                                      ),
                                      TextSpan(text: _legaleseLinkPostfix())
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ],
                          applicationVersion: widget.packageInfo.version);
                    }
                    break;
                  case HomeMenuItem.help:
                    {
                      Navigator.of(context).pushNamed('/help');
                    }
                    break;
                  case HomeMenuItem.export:
                    {
                      MigraineLogExporter(
                        Provider.of<MigraineList>(context, listen: false),
                        state: Provider.of<MigraineLogGlobalState>(context,
                            listen: false),
                        config: Provider.of<MigraineLogConfig>(context,
                            listen: false),
                      ).export();
                    }
                    break;
                  case HomeMenuItem.import:
                    {
                      MigraineLogImportUI(
                          Provider.of<MigraineList>(context, listen: false))
                        ..getFromFilepicker(context);
                    }
                    break;
                  case HomeMenuItem.exit:
                    {
                      var list =
                          Provider.of<MigraineList>(context, listen: false);
                      list.saveData();
                      SystemNavigator.pop();
                    }
                    break;
                  case HomeMenuItem.settings:
                    {
                      Navigator.of(context).pushNamed('/config');
                    }
                    break;
                  case HomeMenuItem.debug:
                    {
                      // When built in kDebugMode this handler is included.
                      // kDebugMode is a constant, so this code block won't be
                      // included in release builds
                      if (kDebugMode) {
                        // The global list
                        var list =
                            Provider.of<MigraineList>(context, listen: false);
                        // The config
                        var config = Provider.of<MigraineLogConfig>(context,
                            listen: false);
                        // Clear the current data
                        for (var date in list.dates()) {
                          var dt = DateTime.parse(date);
                          list.remove(dt);
                        }
                        // Either use the first medication in the config, *or*
                        // our own test one, to generate the data
                        var med = config.medications.takenMeds
                            ? config.medications.list[0]
                            : "TestMedication";
                        // Stash today for comparisons
                        var now = DateTime.now();
                        // We need some random data
                        var rng = Random();
                        // Our iterator, generate 100 days of data
                        var dateIterator = now.subtract(Duration(days: 100));
                        while (dateIterator.isBefore(now)) {
                          // 50% chance for migraine
                          if (rng.nextInt(2) > 0) {
                            var entry = MigraineEntry(parentList: list);
                            entry.date = dateIterator;
                            // Generate the strength (int 1-3)
                            var strength = rng.nextInt(3);
                            strength++;
                            entry.strength =
                                MigraineStrengthConverter.fromNumber(strength);
                            // 10% chance for meds on strength 1
                            if (strength == 1) {
                              if (rng.nextInt(10) == 1) {
                                entry.medications.add(med);
                              }
                            }
                            // 50% chance for meds on strength 2
                            else if (strength == 2) {
                              if (rng.nextInt(2) == 1) {
                                entry.medications.add(med);
                              }
                            }
                            // 75% chance for meds on strength 3
                            else if (strength == 3) {
                              if (rng.nextInt(4) > 0) {
                                entry.medications.add(med);
                              }
                            }
                            // Save the entry
                            list.set(entry);
                          }
                          // Bump our iterator
                          dateIterator = dateIterator.add(Duration(days: 1));
                        }
                      }
                    }
                    break;
                }
              },
              itemBuilder: (BuildContext context) =>
                  <PopupMenuEntry<HomeMenuItem>>[
                PopupMenuItem<HomeMenuItem>(
                  value: HomeMenuItem.about,
                  child: Text(_aboutMessage()),
                ),
                PopupMenuItem<HomeMenuItem>(
                  value: HomeMenuItem.help,
                  child: Text(_helpMessage()),
                ),
                if (kDebugMode)
                  PopupMenuItem<HomeMenuItem>(
                    value: HomeMenuItem.debug,
                    child: Text('Build debug entries'),
                  ),
                PopupMenuItem<HomeMenuItem>(
                  value: HomeMenuItem.settings,
                  child: Text(_settingsMessage()),
                ),
                PopupMenuItem<HomeMenuItem>(
                  value: HomeMenuItem.export,
                  child: Text(_exportMessage()),
                ),
                PopupMenuItem<HomeMenuItem>(
                  value: HomeMenuItem.import,
                  child: Text(_importMessage()),
                ),
                PopupMenuItem<HomeMenuItem>(
                  value: HomeMenuItem.exit,
                  child: Text(_exitMessage()),
                ),
              ],
            ),
          ),
        ],
      ),
      body: TabBarView(
        /// The tabs
        children: [
          MigraineLogStatsLastNDaysViewer(days: 30),
          MigraineLogViewer(),
          MigraineLogStatsViewer(),
        ],
        controller: controller,
      ),
      floatingActionButton: Consumer2<MigraineLogGlobalState, MigraineList>(
          builder: (context, state, list, _) =>
              AddOrEditFAB(state: state, list: list, controller: controller)),
    );
  }
}

/// Lifecycle event handler
/// This is a widget that doesn't render anything, but that listens for events
/// from the OS (ie. Android) on when the app is closed, hidden etc. This is used
/// to force-save our data on those events.
class MigraineLogAppLifecycle extends StatefulWidget {
  final Widget child;
  final MigraineList list;
  final MigraineLogConfig config;

  MigraineLogAppLifecycle({
    @required this.child,
    @required this.list,
    @required this.config,
  });

  @override
  _MigraineLogAppLifecycleState createState() =>
      _MigraineLogAppLifecycleState();
}

class _MigraineLogAppLifecycleState extends State<MigraineLogAppLifecycle>
    with WidgetsBindingObserver {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  /// Save data on almost any AppLifecycleState
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state != AppLifecycleState.resumed) {
      widget.list.saveData();
      widget.config.saveConfig();
    }
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }
}

/// Wrapper that builds our floatingActionButton. This wrapper renders *either*
/// a single FAB, OR a multiFAB that displays edit/delete elements.
class AddOrEditFAB extends StatelessWidget {
  AddOrEditFAB({
    @required this.state,
    @required this.list,
    @required this.controller,
  });
  final MigraineLogGlobalState state;
  final MigraineList list;
  final TabController controller;
  final format = DateFormat(DateRenderFormat, LocaleHack.dateLocale);

  String undoStr() {
    return Intl.message('Undo');
  }

  String deletedDateMessage(String fmtDate) {
    return Intl.message("Deleted $fmtDate",
        args: [fmtDate], name: "deletedDateMessage");
  }

  String deleteMessage() {
    return Intl.message("Delete");
  }

  String editMessage() {
    return Intl.message("Edit");
  }

  String addMessage() {
    return Intl.message("Add");
  }

  String actionMenuTooltipText() {
    return Intl.message("Show action menu");
  }

  void delete(BuildContext context) {
    var currDate = state.currentDate;
    if (list.exists(currDate)) {
      var entry = list.get(currDate);
      list.remove(currDate);
      String fmtDate = format.format(entry.date);
      list.remove(entry.date);

      /// Permit undoing deletion
      Scaffold.of(context).showSnackBar(SnackBar(
        duration: Duration(seconds: 6),
        content: Text(deletedDateMessage(fmtDate)),
        action:
            SnackBarAction(label: undoStr(), onPressed: () => list.set(entry)),
      ));
    }
  }

  void editPressed(BuildContext context) {
    var currDate = state.currentDate;
    var params = MigraineEditorPathParameters(entry: list.get(currDate));
    Navigator.of(context).pushNamed('/editor', arguments: params);
  }

  void addPressed(BuildContext context) {
    var currDate = state.currentDate;
    if (controller.index == 1) {
      var params = MigraineEditorPathParameters(date: currDate);
      Navigator.of(context).pushNamed('/editor', arguments: params);
    } else {
      Navigator.of(context).pushNamed('/editor');
    }
  }

  @override
  Widget build(BuildContext context) {
    var currDate = state.currentDate;

    if (controller.index == 1) {
      if (list.exists(currDate)) {
        return SpeedDial(
            tooltip: actionMenuTooltipText(),
            marginRight: 14,
            overlayOpacity: 0,
            animatedIcon: AnimatedIcons.menu_close,
            animatedIconTheme: IconThemeData(size: 22.0),
            children: [
              /// Delete the current entry
              SpeedDialChild(
                child: Semantics(
                  label: deleteMessage(),
                  child: Icon(Icons.delete),
                ),
                label: deleteMessage(),
                labelStyle: TextStyle(color: Colors.black),
                onTap: () => delete(context),
              ),

              /// Edit the current entry
              SpeedDialChild(
                child: Semantics(
                  label: editMessage(),
                  child: Icon(Icons.edit),
                ),
                label: editMessage(),
                labelStyle: TextStyle(color: Colors.black),
                onTap: () => editPressed(context),
              ),

              /// Add a new entry on the current date
              SpeedDialChild(
                child: Semantics(
                  label: addMessage(),
                  child: Icon(Icons.add),
                ),
                label: addMessage(),
                labelStyle: TextStyle(color: Colors.black),
                onTap: () => addPressed(context),
              ),
            ]);
      }
    }

    return FloatingActionButton(
      onPressed: () => addPressed(context),
      tooltip: addMessage(),
      child: Icon(Icons.add),
      key: Key("fab_add_button"),
    );
  }
}
