// Migraine Log - a simple multi-platform headache diary
// Copyright (C) 2021    Eskild Hustvedt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import 'package:intl/intl.dart';

class MLi18nStrings {
  MLi18nStrings._();

  static String daysPlural(int days) {
    return Intl.message(
      "$days days",
      args: [days],
      name: 'daysPlural',
    );
  }

  static String daySignular(int day) {
    return Intl.message(
      "$day day",
      args: [day],
      name: 'daySignular',
    );
  }

  static String dayString(int day) {
    if (day > 1) {
      return daysPlural(day);
    }
    return daySignular(day);
  }
}
