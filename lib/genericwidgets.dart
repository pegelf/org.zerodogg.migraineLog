// Migraine Log - a simple multi-platform headache diary
// Copyright (C) 2021    Eskild Hustvedt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';
import 'datatypes.dart';
import 'localehack.dart';

/// A very simple header widget (horizontally centered, bold text)
class MigraineLogHeader extends StatelessWidget {
  MigraineLogHeader({Key key, this.text}) : super(key: key);

  final String text;

  @override
  Widget build(BuildContext context) {
    return MDTextBox(
        text: text,
        style: TextStyle(fontWeight: FontWeight.bold),
        align: TextAlign.center);
  }
}

/// A wrapper around Text that adds padding (optionally selected by the user),
/// alignment (optionally selected by the user) and styling (optionally
/// selected by the user). Just providing text will give a left aligned text
/// widget with decent padding.
class MDTextBox extends StatelessWidget {
  static final defaultInsets = 16.0;
  MDTextBox(
      {Key key,
      this.text,
      this.style,
      this.padding,
      this.align,
      this.semanticsLabel})
      : super(key: key);
  final String text;
  final String semanticsLabel;
  final TextStyle style;
  final TextAlign align;
  final EdgeInsets padding;
  final EdgeInsets _defaultPadding = EdgeInsets.all(defaultInsets);

  @override
  Widget build(BuildContext context) {
    return Row(children: [
      Expanded(
        child: Padding(
          padding: padding ?? _defaultPadding,
          child: Text(text,
              textAlign: align ?? TextAlign.left,
              style: style,
              semanticsLabel: semanticsLabel),
        ),
      ),
    ]);
  }
}

class MigraineLogMonthSwitcherWidget extends StatelessWidget {
  MigraineLogMonthSwitcherWidget({
    this.displayedMonth,
  });
  static final double height = 60;
  final _dateFormat = DateFormat('MMM yyyy', LocaleHack.dateLocale);
  final DateTime displayedMonth;

  String _prevMonthMessage() {
    return Intl.message("Previous month",
        desc: "Tooltip for the previous month button");
  }

  String _nextMonthMessage() {
    return Intl.message("Next month",
        desc: "Tooltip for the next month button");
  }

  String _currentMonth(month) {
    return Intl.message("Current month: $month",
        name: '_currentMonth',
        desc:
            "Used for screen readers to describe the element in the month selector that indicates the current month. The month variable will contain a pre-localized month name along with the year",
        args: [month]);
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<MigraineLogGlobalState>(builder: (context, state, _) {
      DateTime viewerMonth = displayedMonth ?? state.currentCalendarMonth;
      // Calculate the next month in advance. We need this to be able to
      // disable the "next" button when we don't allow changing months.
      var nextMonth = DateTime(
        state.currentCalendarMonth.year,
        state.currentCalendarMonth.month + 1,
        1,
      );
      var formattedMonth = _dateFormat.format(viewerMonth);
      return Container(
          child: Row(
        children: [
          SizedBox(
            width: 60,
            child: Tooltip(
              message: _prevMonthMessage(),
              child: IconButton(
                onPressed: () {
                  state.currentCalendarMonth = DateTime(
                    state.currentCalendarMonth.year,
                    state.currentCalendarMonth.month - 1,
                    1,
                  );
                },
                icon: Icon(
                  Icons.arrow_left,
                  size: 40,
                ),
              ),
            ),
          ),
          Expanded(
            child: SizedBox(
              height: 30,
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Text(
                  formattedMonth,
                  semanticsLabel: _currentMonth(formattedMonth),
                  style: TextStyle(fontSize: 20),
                ),
              ),
            ),
          ),
          SizedBox(
            width: 60,
            child: Tooltip(
              message: _nextMonthMessage(),
              child: IconButton(
                // If onPressed is null then the button is disabled. Thus, if
                // nextMonth is after the current date, we disable it by
                // providing a null onPressed callback.
                onPressed: nextMonth.isAfter(DateTime.now())
                    ? null
                    : () {
                        state.currentCalendarMonth = nextMonth;
                      },
                icon: Icon(
                  Icons.arrow_right,
                  size: 40,
                ),
              ),
            ),
          ),
        ],
      ));
    });
  }
}
