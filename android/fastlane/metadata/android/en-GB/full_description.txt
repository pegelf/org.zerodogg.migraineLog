Migraine Log lets you easily log your migraine attacks (or other headaches). It is built to make it as easy as possible, only requiring a date and the strength of the attack, while allowing you to optionally add details like what medications you took and notes about the attack. This lets you monitor your migraine and can assist you and your physician in finding the correct treatment for you.

The user interface is built with migraine sufferers in mind, and uses soft and dark colours so that you can use it during an attack.

Migraine Log respects your privacy and does not request network access, so your information does not leave your device unless you explicitly use the export function in the app.
