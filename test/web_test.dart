// Migraine Log - a simple multi-platform headache diary
// Copyright (C) 2021    Eskild Hustvedt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

@TestOn('browser')

import 'package:matcher/matcher.dart';
import 'package:test/test.dart';
import 'package:MigraineLog/web.dart' as mlw;
import 'dart:html';
import 'dart:convert';

void main() {
  Map testData = {
    "exportVersion": 1,
    "version": "testing",
    "data": {
      "202101": {
        "2021-01-01": {
          "strength": 1,
          "note": "Testnote",
          "medicationList": ["Medication"],
          "date": "Friday, 1. January 2021"
        },
        "2021-01-05": {
          "strength": 2,
          "note": "Testnote",
          "medicationList": ["Medication"],
          "date": "Tuesday, 5. January 2021"
        },
        "2021-01-09": {
          "strength": 3,
          "note": "",
          "medicationList": [],
          "date": "Saturday, 9. January 2021"
        }
      }
    },
    "summaries": {
      "202101": {
        "headacheDays": "3 days",
        "medicationDays": "2 days",
        "specificSummaries": {
          "Strong migraine": "1 day",
          "Migraine": "1 day",
          "Headache": "1 day"
        }
      }
    },
    "strings": {
      "messages": {
        "migraineLog": "Migraine Log",
        "note": "Note",
        "none": "(none)",
        "strength": "Strength",
        "date": "Date",
        "takenMeds": "Medications",
        "totalHeadacheDays": "Total headache days",
        "hide": "hide",
        "hideTooltip": "Hide this month",
        "hidden": "hidden:",
        "months": "months",
        "everything": "everything",
        "show": "show",
        "filter": "Filter:",
      },
      "strength": {"2": "Migraine", "3": "Strong migraine", "1": "Headache"},
      "months": {"202101": "January 2021"}
    }
  };

  group('dataToHTML', () {
    test('Sanity', () {
      var dh = mlw.dataToHTML(data: testData);
      expect(dh.getHumanMonthString("202101"),
          testData["strings"]["months"]["202101"]);
      expect(dh.strength("1"), testData["strings"]["strength"]["1"] + ' (1)');
      expect(dh.message("note"), testData["strings"]["messages"]["note"]);
      expect(dh.getMonth("202101"), testData["data"]["202101"]);
      expect(dh.getMonthSummary("202101"), testData["summaries"]["202101"]);
      expect(dh.medicationListToStr([]), dh.message('none'));
      expect(dh.medicationListToStr(["Hello"]), "Hello");
      expect(dh.medicationListToStr(["Hello", "world"]), "Hello, world");
    });
    test('sorted', () {
      var dh = mlw.dataToHTML(data: testData);
      expect(dh.sorted(["a", "b", "c"]), ["c", "b", "a"]);
    });
    test('monthSummary', () {
      var dh = mlw.dataToHTML(data: testData);
      var result = dh.monthSummary("202101");
      expect(result, TypeMatcher<Element>());
      expect(result.children.length, 5);
      expect(result.innerHtml,
          "<tr><td>Strong migraine</td><td>1 day</td></tr><tr><td>Migraine</td><td>1 day</td></tr><tr><td>Headache</td><td>1 day</td></tr><tr><td>Medications</td><td>2 days</td></tr><tr><td>Total headache days</td><td>3 days</td></tr>");
    });
    test('monthToHTML', () {
      var dh = mlw.dataToHTML(data: testData);
      var result = dh.monthToHTML("202101");
      expect(result, TypeMatcher<Element>());
      expect(result.children.length, 3);
      expect(result.innerHtml,
          '<h2>January 2021<span class="hide-month" title="Hide this month">[hide]</span></h2><table><tr><td>Strong migraine</td><td>1 day</td></tr><tr><td>Migraine</td><td>1 day</td></tr><tr><td>Headache</td><td>1 day</td></tr><tr><td>Medications</td><td>2 days</td></tr><tr><td>Total headache days</td><td>3 days</td></tr></table><table class="table"><thead><tr><th>Date<span class="sortArrow"> ▼</span></th><th>Strength<span class="sortArrow" style="visibility: hidden;"> ▼</span></th><th>Medications<span class="sortArrow" style="visibility: hidden;"> ▼</span></th><th>Note<span class="sortArrow" style="visibility: hidden;"> ▼</span></th></tr></thead><tbody><tr><td>Saturday, 9. January 2021</td><td>Strong migraine (3)</td><td>(none)</td><td></td></tr><tr><td>Tuesday, 5. January 2021</td><td>Migraine (2)</td><td>Medication</td><td>Testnote</td></tr><tr><td>Friday, 1. January 2021</td><td>Headache (1)</td><td>Medication</td><td>Testnote</td></tr></tbody></table>');
    });
  });
  group('monthTable', () {
    test('defaults', () {
      var m = mlw.monthTableManager();
      expect(m.field, mlw.sortField.date);
      expect(m.direction, mlw.sortDirection.desc);
    });
    test('element', () {
      var m = mlw.monthTableManager();
      var e = mlw.monthTable(data: testData, strMonth: '202101', manager: m);
      expect(e.element, TypeMatcher<Element>());
    });
    test('getField', () {
      var m = mlw.monthTableManager();
      var e = mlw.monthTable(data: testData, strMonth: '202101', manager: m);
      expect(e.getField({}, 'test'), '');
      expect(e.getField({'test': 'test'}, 'test'), TypeMatcher<String>());
      expect(e.getField({'test': 'test'}, 'test'), 'test');
      expect(e.getField({'test': 1}, 'test'), TypeMatcher<int>());
      expect(e.getField({'test': 1}, 'test'), 1);
      expect(
          e.getField({
            'test': ['hello', 'world']
          }, 'test'),
          TypeMatcher<String>());
      expect(
          e.getField({
            'test': ['hello', 'world']
          }, 'test'),
          'helloworld');
    });
    test('sortingArrow', () {
      var m = mlw.monthTableManager();
      var e = mlw.monthTable(data: testData, strMonth: '202101', manager: m);
      expect(e.sortingArrow(mlw.sortField.strength), TypeMatcher<Element>());
      expect(e.sortingArrow(mlw.sortField.strength).innerHtml, contains('▼'));
      expect(e.sortingArrow(mlw.sortField.strength).outerHtml,
          contains('sortArrow'));
      expect(e.sortingArrow(mlw.sortField.strength).outerHtml,
          contains('visibility: hidden'));

      expect(e.sortingArrow(mlw.sortField.date).outerHtml,
          isNot(contains('visibility: hidden')));

      m.set(mlw.sortField.strength, mlw.sortDirection.asc);
      expect(e.sortingArrow(mlw.sortField.strength).innerHtml, contains('▲'));
    });
    test('tableHeader', () {
      var m = mlw.monthTableManager();
      var e = mlw.monthTable(data: testData, strMonth: '202101', manager: m);
      expect(e.tableHeader("note", mlw.sortField.note), TypeMatcher<Element>());
      expect(e.tableHeader("note", mlw.sortField.note).innerText,
          contains(e.message("note")));
      expect(
          e.tableHeader("note", mlw.sortField.note).innerText, contains('▼'));

      expect(m.field, mlw.sortField.date);
      expect(m.direction, mlw.sortDirection.desc);
      e.tableHeader("note", mlw.sortField.note).click();
      expect(m.field, mlw.sortField.note);
      expect(m.direction, mlw.sortDirection.desc);
      e.tableHeader("note", mlw.sortField.note).click();
      expect(m.direction, mlw.sortDirection.asc);
      expect(m.field, mlw.sortField.note);
    });
    test('sortByField', () {
      var m = mlw.monthTableManager();
      var e = mlw.monthTable(data: testData, strMonth: '202101', manager: m);
      expect(
          e.sortByField(testData['data']['202101'].keys.toList(),
              testData['data']['202101'], 'strength'),
          ['2021-01-09', '2021-01-05', '2021-01-01']);
      m.set(mlw.sortField.date, mlw.sortDirection.asc);
      expect(
          e.sortByField(testData['data']['202101'].keys.toList(),
              testData['data']['202101'], 'strength'),
          ['2021-01-01', '2021-01-05', '2021-01-09']);
      m.set(mlw.sortField.date, mlw.sortDirection.desc);
      expect(
          e.sortByField(testData['data']['202101'].keys.toList(),
              testData['data']['202101'], 'medicationList'),
          ['2021-01-01', '2021-01-05', '2021-01-09']);
      expect(
          e.sortByField(testData['data']['202101'].keys.toList(),
              testData['data']['202101'], 'note'),
          ['2021-01-01', '2021-01-05', '2021-01-09']);
    });
    test('sortedMonthKeys', () {
      var m = mlw.monthTableManager();
      var e = mlw.monthTable(data: testData, strMonth: '202101', manager: m);
      expect(e.sortedMonthKeys(), ['2021-01-09', '2021-01-05', '2021-01-01']);
      m.set(mlw.sortField.date, mlw.sortDirection.asc);
      expect(e.sortedMonthKeys(), ['2021-01-01', '2021-01-05', '2021-01-09']);

      m.set(mlw.sortField.strength, mlw.sortDirection.desc);
      expect(e.sortedMonthKeys(), ['2021-01-09', '2021-01-05', '2021-01-01']);
      m.set(mlw.sortField.strength, mlw.sortDirection.asc);
      expect(e.sortedMonthKeys(), ['2021-01-01', '2021-01-05', '2021-01-09']);
    });
    test('changeSort', () {
      var m = mlw.monthTableManager();
      var e = mlw.monthTable(data: testData, strMonth: '202101', manager: m);
      expect(m.field, mlw.sortField.date);
      e.changeSort(mlw.sortField.date);
      expect(m.field, mlw.sortField.date);
      expect(m.direction, mlw.sortDirection.asc);
      e.changeSort(mlw.sortField.strength);
      expect(m.direction, mlw.sortDirection.desc);
      expect(m.field, mlw.sortField.strength);
      e.changeSort(mlw.sortField.strength);
      expect(m.direction, mlw.sortDirection.asc);
      expect(m.field, mlw.sortField.strength);
    });
    test('build', () {
      var m = mlw.monthTableManager();
      var e = mlw.monthTable(data: testData, strMonth: '202101', manager: m);
      expect(e.build(), TypeMatcher<Element>());
    });
  });
  test('tableManager', () {
    var called = false;
    var m = mlw.monthTableManager();
    expect(m.field, mlw.sortField.date);
    expect(m.direction, mlw.sortDirection.desc);
    m.listen(() => called = true);
    m.set(mlw.sortField.strength, mlw.sortDirection.desc);
    expect(m.field, mlw.sortField.strength);
    expect(m.direction, mlw.sortDirection.desc);
    expect(called, isTrue);
    m.set(mlw.sortField.date, mlw.sortDirection.asc);
    expect(m.field, mlw.sortField.date);
    expect(m.direction, mlw.sortDirection.asc);
  });
  test('main', () {
    querySelector('body')
      ..append(Element.div()
        ..innerText = jsonEncode(testData)
        ..attributes["id"] = "migraineLogData")
      ..append(Element.div()..attributes["id"] = "loading");
    mlw.main();
    expect(querySelector('#loading'), isNull);
  });
}
