// Migraine Log - a simple multi-platform headache diary
// Copyright (C) 2021    Eskild Hustvedt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import 'dart:io';
import 'package:MigraineLog/datatypes.dart';
import 'package:MigraineLog/definitions.dart';
import 'package:flutter/material.dart' as flutter_mat;
import 'package:flutter_test/flutter_test.dart';
import 'package:matcher/matcher.dart';
import 'package:mockito/mockito.dart';
import 'package:path_provider_platform_interface/path_provider_platform_interface.dart';
import 'package:fake_async/fake_async.dart';
import 'utils.dart';
import 'dart:convert';

class MockMigraineList extends Mock implements MigraineList {}

class DelayedDataWriteTester with DelayedDataWrite {
  int writeCalls = 0;

  @override
  void write() {
    writeCalls++;
  }
}

class SafeDataWriteTester with SafeDataWrite {}

void main() {
  // Needed for the getApplicationDocumentsDirectory mock to work.
  TestWidgetsFlutterBinding.ensureInitialized();

  Directory tmp;

  setUp(() async {
    tmp = await Directory.systemTemp.createTemp();
    PathProviderPlatform.instance = MockPathProviderPlatform(dir: tmp);
    return true;
  });
  tearDown(() async {
    tmp.deleteSync(recursive: true);
    tmp = null;
    return true;
  });

  group('MigraineEntry', () {
    test('Basic instantiation', () {
      var e = MigraineEntry();
      expect(e.date, TypeMatcher<DateTime>());
      expect(e.medications, TypeMatcher<MigraineMedicationList>());
      // Should be null in a new object
      expect(
        e.strength,
        null,
        reason: 'Default strength should be null',
      );
      expect(
        dateString(e.date),
        dateString(DateTime.now()),
        reason: 'Default date should be now',
      );
      expect(
        e.parentList,
        null,
        reason: 'parentList should default to null',
      );
      expect(
        e.medications.parent,
        e,
        reason:
            'The medications parent should be set to the MigraineEntry object that created it',
      );
    });
    test('Instantiation with date', () {
      var dt = DateTime.utc(2018, 9, 8);
      var e = MigraineEntry(date: dt);
      expect(dateString(e.date), dateString(dt));
    });
    test('strength', () {
      var e = MigraineEntry();
      expect(
        e.strength,
        null,
        reason: 'Default strength should be null',
      );
      expect(
        () {
          e.strength = MigraineStrength.migraine;
        },
        returnsNormally,
        reason: "Should be able to set strength",
      );
      expect(
        e.strength,
        MigraineStrength.migraine,
        reason: "The strength should have changed",
      );
    });
    test('date', () {
      var e = MigraineEntry();
      expect(
        dateString(e.date),
        dateString(DateTime.now()),
        reason: 'Default date should be now',
      );
      expect(
        () {
          e.date = DateTime.utc(2018, 9, 8);
        },
        returnsNormally,
        reason: "Should be able to set date",
      );
      expect(
        dateString(e.date),
        dateString(DateTime.utc(2018, 9, 8)),
        reason: "The date should have changed",
      );
    });
    test('note', () {
      var e = MigraineEntry();
      expect(
        e.note,
        '',
        reason: 'Default note should be empty',
      );
      expect(
        () {
          e.note = "Hello World";
        },
        returnsNormally,
        reason: "Should be able to set a note",
      );
      expect(e.note, "Hello World", reason: "The note should have changed");
    });
    test('toMap', () {
      var e = MigraineEntry();
      e.note = "Testnote";
      e.strength = MigraineStrength.strongMigraine;
      var map;
      expect(() {
        map = e.toMap();
      }, returnsNormally);
      expect(map['note'], 'Testnote');
      expect(map['strength'], 3);
      expect(map['medicationList'], isList);
    });
    test('Restoring fromMap', () {
      var dt = DateTime.utc(2018, 9, 8);
      var e = MigraineEntry(date: dt);
      e.note = "Testnote";
      e.strength = MigraineStrength.strongMigraine;
      var restoredE = MigraineEntry.fromMap(e.toMap(), date: dt);
      expect(dateString(restoredE.date), dateString(dt));
      expect(restoredE.note, 'Testnote');
      expect(restoredE.strength, MigraineStrength.strongMigraine);
    });
    test('List interaction', () {
      var list = MockMigraineList();
      var e = MigraineEntry(parentList: list);
      expect(e.parentList, list);
      e.save();
      verify(list.set(e));
      var fromMap = MigraineEntry.fromMap(e.toMap(), parentList: list);
      expect(fromMap.parentList, list);
    });
    test('Cloning', () {
      var e = MigraineEntry();
      e.note = "Testnote";
      e.strength = MigraineStrength.strongMigraine;
      var cloned = e.clone();
      expect(cloned.note, 'Testnote');
      expect(cloned.strength, e.strength);
      cloned.strength = MigraineStrength.headache;
      expect(e.strength, isNot(MigraineStrength.headache));
    });
  });
  group('MigraineMedicationEntry', () {
    test('Creation', () {
      var e = MigraineMedicationEntry(medication: 'Test');
      expect(e.medication, 'Test');
      expect(e.iter, TypeMatcher<int>());
      expect(e.key, TypeMatcher<flutter_mat.Key>());
    });
    test('clone', () {
      var e = MigraineMedicationEntry(medication: 'Test');
      expect(e.medication, 'Test');
      var c = e.clone();
      expect(c.medication, e.medication);
      expect(e.key.toString(), isNot(c.key.toString()));
    });
  });
  group('MigraineMedicationList', () {
    test('parent', () {
      var l = MigraineMedicationList();
      expect(l.parent, isNull);
      var e = MigraineEntry();
      l.parent = e;
      expect(l.parent, e);
      expect(
        () {
          l.parent = l;
        },
        throwsArgumentError,
        reason:
            'Should not allow types other than MigraineEntryand MigraineLogConfig to be set to parent',
      );
      var c = MigraineLogConfig();
      l.parent = c;
      expect(l.parent, c);
    });
    group('toggle', () {
      test('String', () {
        var l = MigraineMedicationList();
        expect(l.has(MigraineMedicationEntry(medication: 'test')), isFalse);
        l.toggle('test');
        expect(l.has(MigraineMedicationEntry(medication: 'test')), isTrue);
      });
      test('MigraineMedicationEntry', () {
        var l = MigraineMedicationList();
        var e = MigraineMedicationEntry(medication: 'test');
        expect(l.has(e), isFalse);
        l.toggle(e);
        expect(l.has(e), isTrue);
      });
    });
    test('.list+add', () {
      var l = MigraineMedicationList();
      expect(l.list, TypeMatcher<List>());
      var e = MigraineMedicationEntry(medication: 'test');
      l.add(e);
      expect(l.list, TypeMatcher<List>());
      expect(l.list.length, 1);
      expect(l.list[0], e);
    });
    test('.length', () {
      var l = MigraineMedicationList();
      var e = MigraineMedicationEntry(medication: 'test');
      var e2 = MigraineMedicationEntry(medication: 'test2');
      var e3 = MigraineMedicationEntry(medication: 'test3');
      expect(l.length, 0);
      expect(l.list.length, l.length);
      l.add(e);
      expect(l.length, 1);
      expect(l.list.length, l.length);
      l.add(e2);
      expect(l.length, 2);
      expect(l.list.length, l.length);
      l.add(e3);
      expect(l.length, 3);
      expect(l.list.length, l.length);
    });
    test('isNotEmpty', () {
      var l = MigraineMedicationList();
      expect(l.isNotEmpty, isFalse);
      var e = MigraineMedicationEntry(medication: 'test');
      l.add(e);
      expect(l.isNotEmpty, isTrue);
    });
    test('reorderByIndex', () {
      var l = MigraineMedicationList();
      var e = MigraineMedicationEntry(medication: 'test');
      var e2 = MigraineMedicationEntry(medication: 'test2');
      var e3 = MigraineMedicationEntry(medication: 'test3');
      l.add(e);
      l.add(e2);
      l.add(e3);
      expect(l.list[0], e);
      expect(l.list[1], e2);
      expect(l.list[2], e3);
      l.reorderByIndex(2, 0);
      expect(l.list[0], e3);
      expect(l.list[1], e);
      expect(l.list[2], e2);
    });
    test('replace', () {
      var l = MigraineMedicationList();
      var e = MigraineMedicationEntry(medication: 'test');
      var e2 = MigraineMedicationEntry(medication: 'test2');
      var e3 = MigraineMedicationEntry(medication: 'test3');
      l.add(e);
      l.add(e2);
      expect(l.list[0], e);
      expect(l.list[1], e2);
      l.replace(e2, e3);
      expect(l.list[0], e);
      expect(l.list[1], e3);
    });
    test('listWithEmpty', () {
      var l = MigraineMedicationList();
      var e = MigraineMedicationEntry(medication: 'test');
      var e2 = MigraineMedicationEntry(medication: 'test2');
      var e3 = MigraineMedicationEntry(medication: 'test3');
      l.add(e);
      l.add(e2);
      l.add(e3);
      expect(l.length, 3);
      expect(l.listWithEmpty.length, 4);
      expect(l.listWithEmpty[0], e);
      expect(l.listWithEmpty[1], e2);
      expect(l.listWithEmpty[2], e3);
      expect(l.listWithEmpty[3].medication, '');
    });
    test('addList', () {
      var l = MigraineMedicationList();
      var e = MigraineMedicationEntry(medication: 'test');
      var e2 = MigraineMedicationEntry(medication: 'test2');
      var e3 = MigraineMedicationEntry(medication: 'test3');
      var e4 = MigraineMedicationEntry(medication: 'test4');
      expect(l.length, 0);
      l.add(e);
      expect(l.length, 1);
      expect(l.list[0], e);
      l.addList([e2, e3, e4]);
      expect(l.length, 4);
      expect(l.list[0], e);
      expect(l.list[1], e2);
      expect(l.list[2], e3);
      expect(l.list[3], e4);
    });
    test('setList', () {
      var l = MigraineMedicationList();
      var e = MigraineMedicationEntry(medication: 'test');
      var e2 = MigraineMedicationEntry(medication: 'test2');
      var e3 = MigraineMedicationEntry(medication: 'test3');
      var e4 = MigraineMedicationEntry(medication: 'test4');
      expect(l.length, 0);
      l.add(e);
      expect(l.length, 1);
      expect(l.list[0], e);
      l.setList([e2, e3, e4]);
      expect(l.length, 3);
      expect(l.list[0], e2);
    });
    test('has', () {
      var l = MigraineMedicationList();
      var e = MigraineMedicationEntry(medication: 'test');
      l.add(e);
      expect(l.list[0], e);
      expect(l.has(e), isTrue);
    });
    group('remove', () {
      test('String', () {
        var l = MigraineMedicationList();
        var e = 'test';
        var eInstance = MigraineMedicationEntry(medication: e);
        expect(l.has(eInstance), isFalse);
        l.add(e);
        expect(l.list.length, 1);
        expect(l.list[0].medication, e);
        expect(l.has(eInstance), isTrue);
        l.remove(e);
        expect(l.has(eInstance), isFalse);
        expect(l.list.length, 0);
      });
      test('MigraineMedicationEntry', () {
        var l = MigraineMedicationList();
        var e = MigraineMedicationEntry(medication: 'test');
        expect(l.has(e), isFalse);
        l.add(e);
        expect(l.list.length, 1);
        expect(l.list[0], e);
        expect(l.has(e), isTrue);
        l.remove(e);
        expect(l.list.length, 0);
        expect(l.has(e), isFalse);
      });
    });
    test('removeEmpty', () {
      var l = MigraineMedicationList();
      var e = MigraineMedicationEntry(medication: 'test');
      var e2 = MigraineMedicationEntry(medication: 'test2');
      var e3 = MigraineMedicationEntry(medication: 'test3');
      var e4 = MigraineMedicationEntry(medication: 'test4');
      l.addList([e, e2, e3, e4]);
      expect(l.length, 4);
      e.medication = '';
      e2.medication = '';
      l.removeEmpty();
      expect(l.length, 2);
      expect(l.list[0], e3);
      expect(l.list[1], e4);
    });
    test('takenMeds', () {
      var l = MigraineMedicationList();
      var e = MigraineMedicationEntry(medication: 'test');
      expect(l.takenMeds, isFalse);
      l.add(e);
      expect(l.takenMeds, isTrue);
      l.remove(e);
      expect(l.takenMeds, isFalse);
    });
    test('toStrList', () {
      var l = MigraineMedicationList();
      var e = MigraineMedicationEntry(medication: 'test');
      var e2 = MigraineMedicationEntry(medication: 'test2');
      var e3 = MigraineMedicationEntry(medication: 'test3');
      var e4 = MigraineMedicationEntry(medication: 'test4');
      l.addList([e, e2, e3, e4]);
      expect(l.toStrList(), ['test', 'test2', 'test3', 'test4']);
    });
    test('toString', () {
      var l = MigraineMedicationList();
      var e = MigraineMedicationEntry(medication: 'test');
      var e2 = MigraineMedicationEntry(medication: 'test2');
      var e3 = MigraineMedicationEntry(medication: 'test3');
      var e4 = MigraineMedicationEntry(medication: 'test4');
      l.addList([e, e2, e3, e4]);
      expect(l.toString(), 'test, test2, test3 and test4');
    });
    test('clone', () {
      var l = MigraineMedicationList();
      var e = MigraineMedicationEntry(medication: 'test');
      l.add(e);
      var c = l.clone();
      expect(l.list[0].medication, 'test');
      expect(c.toString(), l.toString());
      expect(c.length, l.length);
      e.medication = 'test2';
      expect(l.list[0].medication, 'test2');
      expect(c.list[0].medication, 'test');
    });
  });

  group('MigraineList', () {
    test('set+get+exists+isNotEmpty', () {
      var c = MigraineLogConfig();
      var l = MigraineList(config: c);
      expect(l.isNotEmpty, isFalse);
      var now = DateTime.now();
      var yesterday = now.subtract(Duration(days: 1));
      var e = MigraineEntry(date: now);
      var e2 = MigraineEntry(date: now);
      l.set(e);
      expect(l.get(now), e);
      expect(l.get(yesterday), isNull);
      expect(l.exists(now), isTrue);
      expect(l.exists(yesterday), isFalse);
      l.set(e2);
      expect(l.get(now), isNot(e));
      expect(l.get(now), e2);
      expect(l.isNotEmpty, isTrue);
    });
    test('remove', () {
      var c = MigraineLogConfig();
      var l = MigraineList(config: c);
      var now = DateTime.now();
      var e = MigraineEntry(date: now);
      l.set(e);
      expect(l.get(now), e);
      l.remove(now);
      expect(l.get(now), isNull);
      l.remove(now); // Should not throw
    });
    test('stringifyDT', () {
      var dt = DateTime(2021, 2, 17);
      var c = MigraineLogConfig();
      var l = MigraineList(config: c);
      expect(l.stringifyDT(dt), '2021-02-17');
    });
    test('dates', () {
      var c = MigraineLogConfig();
      var l = MigraineList(config: c);
      var now = DateTime.now();
      var yesterday = now.subtract(Duration(days: 1));
      var e = MigraineEntry(date: now);
      var e2 = MigraineEntry(date: yesterday);
      l.set(e);
      l.set(e2);
      var dates = l.dates();
      dates.sort();
      expect(dates, [l.stringifyDT(yesterday), l.stringifyDT(now)]);
    });
    test('entries', () {
      var c = MigraineLogConfig();
      var l = MigraineList(config: c);
      var now = DateTime.now();
      var yesterday = now.subtract(Duration(days: 1));
      var e = MigraineEntry(date: now);
      var e2 = MigraineEntry(date: yesterday);
      expect(l.entries, 0);
      l.set(e);
      expect(l.entries, 1);
      l.set(e2);
      expect(l.entries, 2);
      l.remove(e2.date);
      expect(l.entries, 1);
    });
    test('dataFile', () async {
      var l = MigraineList(config: MigraineLogConfig());
      var dFile = await l.dataFile();
      expect(dFile, TypeMatcher<File>());
      expect(dFile.path, matches('.+/list.json'));
    });
    test('loadData', () async {
      var l = MigraineList(config: MigraineLogConfig());
      var dFile = await l.dataFile();
      // Security cleanup in case some other test has already created it
      if (dFile.existsSync()) {
        dFile.deleteSync();
      }
      // loadData with no file that exists, should not crash.
      await l.loadData();
      var dataDate = DateTime(2021, 2, 17);
      await dFile.writeAsString(
        jsonEncode(
          {
            "dataVersion": 1,
            "entries": {
              "2021-02-17": {
                "strength": 1,
                "note": "testNote",
                "medicationList": ["testMed"],
              },
            },
          },
        ),
      );
      expect(l.entries, 0);
      expect(l.exists(dataDate), isFalse);
      await l.loadData();
      expect(l.exists(dataDate), isTrue);
      expect(l.entries, 1);
      expect(l.get(dataDate).note, "testNote");
      expect(l.get(dataDate).strength, MigraineStrength.headache);
      expect(
          l
              .get(dataDate)
              .medications
              .has(MigraineMedicationEntry(medication: "testMed")),
          isTrue);
    });
    test('saveData', () async {
      var l = MigraineList(config: MigraineLogConfig());
      var dFile = await l.dataFile();
      var e = MigraineEntry();
      e.note = "Testnote";
      e.strength = MigraineStrength.strongMigraine;
      e.date = DateTime(2021, 2, 17);
      e.medications.toggle("testMed");
      l.set(e);
      await l.saveData();
      expect(
        jsonDecode(await dFile.readAsString()),
        {
          "dataVersion": 1,
          "entries": {
            "2021-02-17": {
              "strength": 3,
              "note": "Testnote",
              "medicationList": ["testMed"],
            },
          },
        },
      );
      expect(await dFile.exists(), isTrue);
    });
    test('saveData stress test', () async {
      bool toggleStatus = false;
      var l = MigraineList(config: MigraineLogConfig());
      var dFile = await l.dataFile();
      var e = MigraineEntry();
      e.note = "Testnote";
      e.strength = MigraineStrength.strongMigraine;
      e.date = DateTime(2021, 2, 17);
      l.set(e);
      for (int i = 0; i < 30; i++) {
        List<String> medState = [];
        if (toggleStatus == true) {
          e.medications.add("testMed");
          medState.add("testMed");
        } else {
          e.medications.remove("testMed");
          await l.saveData();
          expect(await dFile.exists(), isTrue);
          expect(
            jsonDecode(await dFile.readAsString()),
            {
              "dataVersion": 1,
              "entries": {
                "2021-02-17": {
                  "strength": 3,
                  "note": "Testnote",
                  "medicationList": medState
                },
              },
            },
          );
        }
      }
    });
    test('write', () async {
      var l = MigraineList(config: MigraineLogConfig());
      var dFile = await l.dataFile();

      var e = MigraineEntry();
      e.note = "Testnote";
      e.strength = MigraineStrength.strongMigraine;
      e.date = DateTime(2021, 2, 17);
      e.medications.toggle("testMed");
      l.set(e);

      expect(await dFile.exists(), isFalse);

      await l.write();

      expect(await dFile.exists(), isTrue);
    });
    test('newEntry', () {
      var l = MigraineList(config: MigraineLogConfig());
      var dt = DateTime(2021, 2, 17);
      var newEntry = l.newEntry(date: dt);
      expect(newEntry, TypeMatcher<MigraineEntry>());
      expect(newEntry.parentList, l);
      expect(newEntry.date, dt);
    });
    test('headacheDaysStats', () {
      var l = MigraineList(config: MigraineLogConfig());
      var now = DateTime.now();

      l.set(MigraineEntry.fromMap({
        "strength": 3,
        "medicationList": ['test1', 'test2'],
        "note": 'note'
      }, date: now, parentList: l));
      l.set(MigraineEntry.fromMap({
        "strength": 2,
        "medicationList": ['test1', 'test2'],
        "note": 'note'
      }, date: now.subtract(Duration(days: 1)), parentList: l));
      l.set(MigraineEntry.fromMap({
        "strength": 1,
        "medicationList": ['test1', 'test2'],
        "note": 'note'
      }, date: now.subtract(Duration(days: 2)), parentList: l));
      l.set(MigraineEntry.fromMap({"strength": 1, "note": 'note'},
          date: now.subtract(Duration(days: 3)), parentList: l));
      l.set(MigraineEntry.fromMap({
        "strength": 2,
      }, date: now.subtract(Duration(days: 4)), parentList: l));

      var stats = l.headacheDaysStats(30);
      expect(stats, TypeMatcher<MigraineLogStatisticsList>());
      expect(stats.entries.length, 4);
      expect(stats.entries[0].strength, null);
      expect(stats.entries[0].days, 25);
      expect(stats.entries[0].ofDays, 30);
      expect(stats.entries[1].strength, MigraineStrength.strongMigraine);
      expect(stats.entries[1].days, 1);
      expect(stats.entries[1].ofDays, 30);
      expect(stats.entries[2].strength, MigraineStrength.migraine);
      expect(stats.entries[2].days, 2);
      expect(stats.entries[2].ofDays, 30);
      expect(stats.entries[3].strength, MigraineStrength.headache);
      expect(stats.entries[3].days, 2);
      expect(stats.entries[3].ofDays, 30);
      expect(stats.headacheDays, 5);
    });
  });
  group('MigraineLogStatisticsList', () {
    test('construction', () {
      var l = MigraineLogStatisticsList();
      expect(l.ofDays, 0);
      expect(l.entries, TypeMatcher<List>());
      var lOD = MigraineLogStatisticsList(ofDays: 30);
      expect(lOD.ofDays, 30);
    });
    test('headacheDays,headachePercentage,medicationPercentage', () {
      var l = MigraineLogStatisticsList(ofDays: 30);
      l.entries.addAll([
        MigraineLogStatisticsEntry(
            title: 'headache',
            days: 1,
            strength: MigraineStrength.headache,
            ofDays: 30),
        MigraineLogStatisticsEntry(
            title: 'migraine',
            days: 1,
            strength: MigraineStrength.migraine,
            ofDays: 30),
        MigraineLogStatisticsEntry(
            title: 'strongMigraine',
            days: 1,
            strength: MigraineStrength.strongMigraine,
            ofDays: 30),
      ]);
      expect(l.headacheDays, 3);
      expect(l.headachePercentage, 10);
      expect(l.medicationPercentage, 0);
      l.medicationDays = 3;
      expect(l.medicationPercentage, 10);
    });
  });
  group('MigraineLogStatisticsEntry', () {
    test('empty construction', () {
      var e = MigraineLogStatisticsEntry();
      expect(e.strength, null);
      expect(e.title, null);
      expect(e.ofDays, 0);
      expect(e.days, 0);
      expect(e.medicationDays, 0);
      expect(e.percentageOfDays, 0);
    });
    test('strength=null construction', () {
      var e = MigraineLogStatisticsEntry(title: "None", days: 25, ofDays: 30);
      expect(e.strength, null);
      expect(e.title, "None");
      expect(e.ofDays, 30);
      expect(e.days, 25);
      expect(e.medicationDays, 0);
      expect(e.percentageOfDays.toInt(), 83);
    });
    test('strength=MigraineStrength.headache construction', () {
      var e = MigraineLogStatisticsEntry(
        title: "None",
        days: 5,
        ofDays: 30,
        strength: MigraineStrength.headache,
      );
      expect(e.strength, MigraineStrength.headache);
      expect(e.title, "None");
      expect(e.ofDays, 30);
      expect(e.days, 5);
      expect(e.medicationDays, 0);
      expect(e.percentageOfDays.toInt(), 16);
    });
  });
  test('MigraineLogGlobalState', () {
    var s = MigraineLogGlobalState();
    var now = DateTime.now();
    expect(s.currentDate, TypeMatcher<DateTime>());
    var sC = s.currentDate;
    var previousDate = DateTime(2021, 2, 17);
    expect([sC.year, sC.month, sC.day].join('-'),
        [now.year, now.month, now.day].join('-'),
        reason: "Should default to today");
    s.currentDate = previousDate;
    expect(s.currentDate, previousDate);
    expect(s.version, null);
    s.version = "0.0.1";
    expect(s.version, "0.0.1");
  });
  test('MigraineLogConfigMessages', () {
    var cm = MigraineLogConfigMessages();
    expect(cm.neutralLanguage, isFalse);
    expect(cm.headacheMessage(), 'Headache');
    expect(cm.migraineMesssage(), 'Migraine');
    expect(cm.strongMigraineMessage(), 'Strong migraine');
    cm.neutralLanguage = true;
    expect(cm.neutralLanguage, isTrue);
    expect(cm.headacheMessage(), 'Mild headache');
    expect(cm.migraineMesssage(), 'Moderate headache');
    expect(cm.strongMigraineMessage(), 'Strong headache');
  });
  group('MigraineLogConfig', () {
    test('construction', () {
      var c = MigraineLogConfig();
      expect(c.medications, TypeMatcher<MigraineMedicationList>());
      expect(c.medications.parent, c);
      expect(c.messages, TypeMatcher<MigraineLogConfigMessages>());
      expect(c.onboardingVersion, 0);
    });
    test('configFilePath', () async {
      var c = MigraineLogConfig();
      var cfile = await c.configFilePath();
      expect(cfile, TypeMatcher<File>());
      expect(cfile.path, matches('.+/config.json'));
    });
    test('onboardingVersion', () {
      var c = MigraineLogConfig();
      expect(c.onboardingVersion, 0);
      c.onboardingVersion = 1;
      expect(c.onboardingVersion, 1);
    });
    test('neutralLanguage', () {
      var c = MigraineLogConfig();
      expect(c.neutralLanguage, isFalse);
      expect(c.messages.neutralLanguage, isFalse);
      c.neutralLanguage = true;
      expect(c.neutralLanguage, isTrue);
      expect(c.messages.neutralLanguage, isTrue);
      c.neutralLanguage = false;
      expect(c.neutralLanguage, isFalse);
      expect(c.messages.neutralLanguage, isFalse);
    });
    test('onboardingDone w/neutralLanguage=false', () {
      var c = MigraineLogConfig();
      c.onboardingDone(1);
      expect(
          c.medications.has(
              MigraineMedicationEntry(medication: c.genericSeizureMedString())),
          isFalse);
      expect(
          c.medications
              .has(MigraineMedicationEntry(medication: c.migraineMedString())),
          isTrue);
      expect(
          c.medications.has(
              MigraineMedicationEntry(medication: c.analgesicsMedString())),
          isTrue);
    });
    test('onboardingDone w/neutralLanguage=true', () {
      var c = MigraineLogConfig();
      c.neutralLanguage = true;
      c.onboardingDone(1);
      expect(
          c.medications
              .has(MigraineMedicationEntry(medication: c.migraineMedString())),
          isFalse);
      expect(
          c.medications.has(
              MigraineMedicationEntry(medication: c.genericSeizureMedString())),
          isTrue);
      expect(
          c.medications.has(
              MigraineMedicationEntry(medication: c.analgesicsMedString())),
          isTrue);
    });
    test('saveConfig', () async {
      var c = MigraineLogConfig();
      c.neutralLanguage = true;
      await c.saveConfig();
      var cfile = await c.configFilePath();
      expect(cfile, TypeMatcher<File>());
      expect(await cfile.exists(), isTrue);
      expect(
        jsonDecode(await cfile.readAsString()),
        {
          "onboardingVersion": 0,
          "medication": [],
          "neutralLanguage": true,
          "configVersion": 1,
        },
      );
      c.medications.add("Test");
      c.onboardingVersion = 2;
      await c.saveConfig();
      expect(
        jsonDecode(await cfile.readAsString()),
        {
          "onboardingVersion": 2,
          "medication": ["Test"],
          "neutralLanguage": true,
          "configVersion": 1,
        },
      );
    });
    test('saveConfig stress test', () async {
      var c = MigraineLogConfig();
      c.neutralLanguage = true;
      await c.saveConfig();
      var cfile = await c.configFilePath();
      c.medications.add("Test");
      for (int i = 1; i < 31; i++) {
        c.onboardingVersion = i;
        await c.saveConfig();
        expect(
          jsonDecode(await cfile.readAsString()),
          {
            "onboardingVersion": i,
            "medication": ["Test"],
            "neutralLanguage": true,
            "configVersion": 1,
          },
        );
      }
    });
    test('loadConfig', () async {
      var c = MigraineLogConfig();
      var cfile = await c.configFilePath();
      expect(await cfile.exists(), isFalse);
      await c.loadConfig(); // Should succeed even when there's no file
      await cfile.writeAsString(jsonEncode({
        "onboardingVersion": 1,
        "medication": ["Testit"],
        "neutralLanguage": true,
        "configVersion": 1,
      }));
      await c.loadConfig();
      expect(c.neutralLanguage, isTrue);
      expect(c.onboardingVersion, 1);
      expect(c.medications.has(MigraineMedicationEntry(medication: "Testit")),
          isTrue);
    });
  });
  test('MigraineEditorPathParameters', () {
    var now = DateTime.now();
    var entry = MigraineEntry();
    var p = MigraineEditorPathParameters();
    expect(p.date, isNull);
    expect(p.entry, isNull);
    p = MigraineEditorPathParameters(date: now);
    expect(p.date, now);
    expect(p.entry, isNull);
    p = MigraineEditorPathParameters(date: now, entry: entry);
    expect(p.date, now);
    expect(p.entry, entry);
    p = MigraineEditorPathParameters(entry: entry);
    expect(p.date, isNull);
    expect(p.entry, entry);
  });
  test('DelayedDataWrite', () {
    fakeAsync((async) {
      var w = DelayedDataWriteTester();
      w.queueWrite(delayMinutes: 1);
      expect(w.writeCalls, 0);
      async.elapse(Duration(seconds: 30));
      expect(w.writeCalls, 0);
      async.elapse(Duration(seconds: 30));
      expect(w.writeCalls, 1);
    });
  });
  group('SafeDataWrite', () {
    test('Writing', () async {
      var tmpFile = File(tmp.path + '/test');
      var dotNew = File(tmp.path + '/test.new');
      var tmpBackupFile = File(tmp.path + '/test~');
      var tester = SafeDataWriteTester();
      expect(tmpFile.existsSync(), false, reason: 'No files exist by default');
      expect(
        tmpBackupFile.existsSync(),
        false,
        reason: 'No files exist by default',
      );
      await tester.writeStringToFile("TEST", tmpFile);
      expect(
        tmpFile.existsSync(),
        true,
        reason: 'File should have been written',
      );
      expect(
        await tmpFile.readAsString(),
        'TEST',
        reason: 'File contents should be as expected',
      );
      expect(
        dotNew.existsSync(),
        false,
        reason: '.new should not exist',
      );
      await tester.writeStringToFile("NEW_FILE", tmpFile);
      expect(
        tmpFile.existsSync(),
        true,
        reason: 'New file should exist',
      );
      expect(
        await tmpFile.readAsString(),
        'NEW_FILE',
        reason: 'File content should have been overwritten',
      );
      expect(
        tmpBackupFile.existsSync(),
        true,
        reason: 'Backup file should exist',
      );
      expect(
        dotNew.existsSync(),
        false,
        reason: '.new should not exist',
      );
      expect(
        await tmpBackupFile.readAsString(),
        'TEST',
        reason: 'Backup content should be as expected',
      );
      await tester.writeStringToFile(
        "THIRD",
        tmpFile,
      );
      expect(
        dotNew.existsSync(),
        false,
        reason: '.new should not exist',
      );
      expect(
        tmpFile.existsSync(),
        true,
      );
      expect(
        await tmpFile.readAsString(),
        'THIRD',
        reason: 'Reading new file should work',
      );
      expect(
        tmpBackupFile.existsSync(),
        true,
      );
      expect(
        await tmpBackupFile.readAsString(),
        'NEW_FILE',
        reason: 'Reading old file should work as expected',
      );
    });
    // Writes a file 30 times with increasing numbers, validating the data
    // written, storage of the backup file, and non-existance of the temporary
    // "new" file.
    test('Writing stress test', () async {
      var tmpFile = File(tmp.path + '/test');
      var tmpBackupFile = File(tmp.path + '/test~');
      var tester = SafeDataWriteTester();
      var dotNew = File(tmp.path + '/test.new');
      for (int i = 0; i < 30; i++) {
        await tester.writeStringToFile(i.toString(), tmpFile);
        expect(await tmpFile.exists(), isTrue,
            reason: "The file should have been written");
        expect(await tmpFile.readAsString(), i.toString(),
            reason: "The data should match");
        expect(await dotNew.exists(), isFalse,
            reason: "The .new should no longer exist");
        if (i > 0) {
          int prev = i - 1;
          expect(await tmpBackupFile.exists(), isTrue,
              reason:
                  "We should have saved the previous file as a backup file");
          expect(await tmpBackupFile.readAsString(), prev.toString(),
              reason: "The backup file's contents should match");
        }
      }
    });
    group('filePathOrFallback', () {
      test('With existing file', () async {
        var tester = SafeDataWriteTester();
        var tmpFile = File(tmp.path + '/test');
        await tmpFile.writeAsString('test');
        expect(tester.filePathOrFallback(tmpFile).path, tmpFile.path);
      });
      test('Without existing file', () async {
        var tester = SafeDataWriteTester();
        var tmpFile = File(tmp.path + '/test');
        expect(tester.filePathOrFallback(tmpFile).path, tmpFile.path);
      });
      test('With only backup file', () async {
        var tester = SafeDataWriteTester();
        var tmpFile = File(tmp.path + '/test');
        var tmpFileBackup = File(tmp.path + '/test~');
        await tmpFileBackup.writeAsString('TEST');
        expect(tester.filePathOrFallback(tmpFile).path, tmpFileBackup.path);
      });
      test('With only .new file', () async {
        var tester = SafeDataWriteTester();
        var tmpFile = File(tmp.path + '/test');
        var tmpFileBackup = File(tmp.path + '/test.new');
        await tmpFileBackup.writeAsString('TEST');
        expect(tester.filePathOrFallback(tmpFile).path, tmpFileBackup.path);
      });
      test('With all files', () async {
        var tester = SafeDataWriteTester();
        var tmpFile = File(tmp.path + '/test');
        await tmpFile.writeAsString('TEST');
        var tmpFileBackup = File(tmp.path + '/test~');
        await tmpFileBackup.writeAsString('TEST');
        var tmpFileNew = File(tmp.path + '/test.new');
        await tmpFileNew.writeAsString('TEST');
        expect(tester.filePathOrFallback(tmpFile).path, tmpFile.path);
      });
    });
  });
}
