// Migraine Log - a simple multi-platform headache diary
// Copyright (C) 2021    Eskild Hustvedt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import 'package:flutter_test/flutter_test.dart';
import 'package:flutter/material.dart';
import 'package:MigraineLog/config.dart';
import 'package:MigraineLog/datatypes.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';
import 'utils.dart';

void main() {
  setTestFile('config');

  group('ConfigMedicationsEntry', () {
    testWidgets('golden', (WidgetTester tester) async {
      var l = MigraineMedicationList();
      var e = MigraineMedicationEntry(medication: 'Test');
      l.add(e);
      await goldenTest(
        widgetType: ConfigMedicationsEntry,
        widgetInstance: ConfigMedicationsEntry(entry: e, list: l),
        tester: tester,
      );
    });
    testWidgets('Typing', (WidgetTester tester) async {
      var l = MigraineMedicationList();
      var e = MigraineMedicationEntry(medication: 'Test');
      l.add(e);
      await tester.pumpWidget(
        materialWidget(
          ConfigMedicationsEntry(entry: e, list: l),
        ),
      );
      expect(e.medication, 'Test');
      await tester.enterText(find.byType(TextField), "Hello");
      await tester.pump();
      expect(e.medication, 'Hello');
    });
    testWidgets('Deleting', (WidgetTester tester) async {
      var l = MigraineMedicationList();
      var e = MigraineMedicationEntry(medication: 'Test');
      l.add(e);
      await tester.pumpWidget(
        materialWidget(
          ConfigMedicationsEntry(entry: e, list: l),
        ),
      );
      expect(l.has(e), isTrue);
      await tester.drag(find.byType(Dismissible), Offset(500.0, 0.0));
      await tester.pumpAndSettle();
      expect(l.has(e), isFalse);
    });
    testWidgets('Creating new', (WidgetTester tester) async {
      var l = MigraineMedicationList();
      var e = MigraineMedicationEntry(medication: '');
      await tester.pumpWidget(
        materialWidget(
          ConfigMedicationsEntry(entry: e, list: l),
        ),
      );
      expect(l.has(e), isFalse);
      await tester.enterText(find.byType(TextField), "Hello");
      await tester.pump();
      expect(l.has(e), isTrue);
    });
    testWidgets('Auto-cleaning empty elements', (WidgetTester tester) async {
      var l = MockMigraineMedicationList();
      when(l.has(any)).thenReturn(true);
      var e = MigraineMedicationEntry(medication: 'Test');
      l.add(e);
      await tester.pumpWidget(
        materialWidget(
          ConfigMedicationsEntry(entry: e, list: l),
        ),
      );
      await tester.enterText(find.byType(TextField), "");
      await tester.pump();
      verify(l.removeEmpty());
    });
  });
  group('MigraineLogNeutralLanguageOnboarding', () {
    testWidgets('golden', (WidgetTester tester) async {
      var c = MigraineLogConfig();
      await goldenTest(
        widgetType: MigraineLogNeutralLanguageOnboarding,
        widgetInstance: MigraineLogNeutralLanguageOnboarding(config: c),
        tester: tester,
      );
    });
    testWidgets('Changing', (WidgetTester tester) async {
      var c = MigraineLogConfig();
      var i = MigraineLogNeutralLanguageOnboarding(config: c);
      await tester.pumpWidget(
        // Why so much boilerplate? Because we need for
        // MigraineLogNeutralLanguageOnboarding to be rebuilt when `c`
        // changes.
        materialWidgetBuilder(
          (context) => Builder(
            builder: (context) => Consumer<MigraineLogConfig>(
              builder: (context, MigraineLogConfig config, _) =>
                  MigraineLogNeutralLanguageOnboarding(config: config),
            ),
          ),
          provide: [
            ChangeNotifierProvider.value(value: c),
          ],
        ),
      );

      expect(
          find.byWidgetPredicate((w) => w is RadioListTile), findsNWidgets(2));
      expect(c.neutralLanguage, isFalse);

      await tester.tap(find.text(i.otherMessage()));
      await tester.pump();
      expect(c.neutralLanguage, isTrue);

      expect(
          find.byWidgetPredicate((w) => w is RadioListTile && w.value == false),
          findsOneWidget);
      await tester.tap(find
          .byWidgetPredicate((w) => w is RadioListTile && w.value == false));
      expect(c.neutralLanguage, isFalse);

      await tester.flush();
    });
  });
  group('MigraineLogNeutralLanguageConfig', () {
    testWidgets('golden', (WidgetTester tester) async {
      var c = MigraineLogConfig();
      await goldenTest(
        widgetType: MigraineLogNeutralLanguageConfig,
        widgetInstance: MigraineLogNeutralLanguageConfig(config: c),
        tester: tester,
      );
    });
    testWidgets('Changing', (WidgetTester tester) async {
      var c = MigraineLogConfig();
      await tester.pumpWidget(
        // Why so much boilerplate? Because we need for
        // MigraineLogNeutralLanguageConfig to be rebuilt when `c`
        // changes.
        materialWidgetBuilder(
          (context) => Builder(
            builder: (context) => Consumer<MigraineLogConfig>(
              builder: (context, MigraineLogConfig config, _) =>
                  MigraineLogNeutralLanguageConfig(config: config),
            ),
          ),
          provide: [
            ChangeNotifierProvider.value(value: c),
          ],
        ),
      );

      expect(c.neutralLanguage, isFalse);

      await tester.tap(find.byWidgetPredicate((w) => w is SwitchListTile));
      await tester.pump();
      expect(c.neutralLanguage, isTrue);

      await tester.tap(find.byWidgetPredicate((w) => w is SwitchListTile));
      await tester.pump();
      expect(c.neutralLanguage, isFalse);

      await tester.flush();
    });
  });
  group('MigraineLogFirstTimeConfig', () {
    testWidgets('golden', (WidgetTester tester) async {
      var c = MigraineLogConfig();
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineLogFirstTimeConfig(),
        provide: [
          ChangeNotifierProvider.value(value: c),
        ],
      ));
      await goldenTest(
        widgetType: MigraineLogFirstTimeConfig,
        name: 'MigraineLogFirstTimeConfig',
        tester: tester,
      );
    });
    testWidgets('Continue', (WidgetTester tester) async {
      MigraineLogConfig c = MockConfig();
      when(c.neutralLanguage).thenReturn(false);
      when(c.medications).thenReturn(MigraineMedicationList());
      await tester.pumpWidget(materialWidgetBuilder(
        (context) => MigraineLogFirstTimeConfig(),
        provide: [
          ChangeNotifierProvider.value(value: c),
        ],
      ));
      verifyNever(c.onboardingDone(any));
      await tester.tap(find.text('Continue'));
      await tester.pump();
      verify(c.onboardingDone(1));
    });
  });
  testWidgets('MigraineLogConfigScreen golden', (WidgetTester tester) async {
    var c = MigraineLogConfig();
    await tester.pumpWidget(materialWidgetBuilder(
      (context) => MigraineLogConfigScreen(),
      provide: [
        ChangeNotifierProvider.value(value: c),
      ],
    ));
    await goldenTest(
      widgetType: MigraineLogConfigScreen,
      name: 'MigraineLogConfigScreen',
      tester: tester,
    );
  });
}

class MockMigraineMedicationList extends Mock
    implements MigraineMedicationList {}

class MockConfig extends Mock implements MigraineLogConfig {}
