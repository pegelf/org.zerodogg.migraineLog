// Migraine Log - a simple multi-platform headache diary
// Copyright (C) 2021    Eskild Hustvedt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import 'package:flutter_test/flutter_test.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:intl/date_symbol_data_local.dart';
import 'package:flutter/material.dart' hide TypeMatcher;
import 'package:MigraineLog/datatypes.dart';
import 'package:MigraineLog/definitions.dart';
import 'package:MigraineLog/stats.dart';
import 'package:matcher/matcher.dart';
import 'package:provider/provider.dart';
import 'utils.dart';

void main() {
  setTestFile('stats');

  var headache = MigraineLogStatisticsEntry(
    title: 'Headache',
    days: 5,
    ofDays: 30,
    strength: MigraineStrength.headache,
  );
  var migraine = MigraineLogStatisticsEntry(
    title: 'Migraine',
    days: 5,
    ofDays: 30,
    strength: MigraineStrength.migraine,
  );
  var strongMigraine = MigraineLogStatisticsEntry(
    title: 'Strong migraine',
    days: 5,
    ofDays: 30,
    strength: MigraineStrength.strongMigraine,
  );
  var other = MigraineLogStatisticsEntry(
    title: 'Nothing',
    days: 15,
    ofDays: 30,
    strength: null,
  );
  List<MigraineLogStatisticsEntry> list = [
    headache,
    migraine,
    strongMigraine,
    other,
  ];

  testWidgets('MigraineLogPieChart golden', (WidgetTester tester) async {
    await goldenTest(
      widgetType: MigraineLogPieChart,
      widgetInstance: MigraineLogPieChart(list: list),
      tester: tester,
    );
  });

  group('MigraineLogTableStats', () {
    testWidgets('golden', (WidgetTester tester) async {
      var stats = MigraineLogStatisticsList(ofDays: 30);
      stats.entries = list;
      await goldenTest(
        widgetType: MigraineLogTableStats,
        widgetInstance: MigraineLogTableStats(stats: stats),
        tester: tester,
      );
    });
    testWidgets('Full', (WidgetTester tester) async {
      var stats = MigraineLogStatisticsList(ofDays: 30);
      stats.medicationDays = 2;
      stats.entries = list;
      await tester.pumpWidget(
        materialWidget(
          MigraineLogTableStats(stats: stats),
        ),
      );
      expect(find.text('Taken medication'), findsOneWidget);
      expect(find.text('Total headache days'), findsOneWidget);
      for (var el in stats.headacheEntires) {
        expect(find.text(el.title), findsOneWidget);
      }
    });
    testWidgets('No meds', (WidgetTester tester) async {
      var stats = MigraineLogStatisticsList(ofDays: 30);
      stats.medicationDays = 0;
      stats.entries = list;
      await tester.pumpWidget(
        materialWidget(
          MigraineLogTableStats(stats: stats),
        ),
      );
      expect(find.text('Taken medication'), findsNothing);
      expect(find.text('Total headache days'), findsOneWidget);
      for (var el in stats.headacheEntires) {
        expect(find.text(el.title), findsOneWidget);
      }
    });
    testWidgets('Partial', (WidgetTester tester) async {
      var stats = MigraineLogStatisticsList(ofDays: 30);
      stats.medicationDays = 2;
      stats.entries = [headache, other];
      await tester.pumpWidget(
        materialWidget(
          MigraineLogTableStats(stats: stats),
        ),
      );
      for (var el in stats.headacheEntires) {
        expect(find.text(el.title), findsOneWidget);
      }
      expect(find.text('Taken medication'), findsOneWidget);
      expect(find.text('Total headache days'), findsOneWidget);
      for (var el in list) {
        if (!stats.entries.contains(el)) {
          expect(find.text(el.title), findsNothing);
        }
      }
    });
    testWidgets('Calculation', (WidgetTester tester) async {
      var stats = MigraineLogStatisticsList(ofDays: 30);
      stats.medicationDays = 2;
      stats.entries = [headache, migraine, strongMigraine, other];
      await tester.pumpWidget(
        materialWidget(
          MigraineLogTableStats(stats: stats),
        ),
      );
      for (var el in stats.headacheEntires) {
        expect(find.text(el.title), findsOneWidget);
      }
      expect(find.text('2 days  (7%)'), findsOneWidget);
      expect(find.text('5 days  (17%)'), findsNWidgets(3));
      expect(find.text('15 days  (50%)'), findsOneWidget);
    });
  });
  group('MigraineLogStatsViewer', () {
    testWidgets('golden with content', (WidgetTester tester) async {
      await initializeDateFormatting("en", null);
      var s = MigraineLogGlobalState();
      var c = MigraineLogConfig();
      var l = MigraineList(config: c);
      var day1 = DateTime(2021, 1, 1);
      var day2 = DateTime(2021, 1, 2);
      var e = MigraineEntry(date: day1);
      var e2 = MigraineEntry(date: day2);
      e.strength = MigraineStrength.migraine;
      e2.strength = MigraineStrength.headache;
      l.set(e);
      l.set(e2);
      s.currentCalendarMonth = day2;
      s.currentDate = day2;
      await tester.pumpWidget(materialWidgetBuilder(
        (_) => MigraineLogStatsViewer(),
        provide: [
          ChangeNotifierProvider.value(value: l),
          ChangeNotifierProvider.value(value: c),
          ChangeNotifierProvider.value(value: s),
        ],
      ));
      await goldenTest(
        widgetType: MigraineLogStatsViewer,
        name: 'MigraineLogStatsViewer.content',
        tester: tester,
      );
      await tester.flush();
    });
    testWidgets('golden without content', (WidgetTester tester) async {
      await initializeDateFormatting("en", null);
      var s = MigraineLogGlobalState();
      var c = MigraineLogConfig();
      var l = MigraineList(config: c);
      await tester.pumpWidget(materialWidgetBuilder(
        (_) => MigraineLogStatsViewer(),
        provide: [
          ChangeNotifierProvider.value(value: l),
          ChangeNotifierProvider.value(value: c),
          ChangeNotifierProvider.value(value: s),
        ],
      ));
      await goldenTest(
        widgetType: MigraineLogStatsViewer,
        name: 'MigraineLogStatsViewer.empty',
        tester: tester,
      );
      await tester.flush();
    });
  });
  test('toChartColor', () {
    var oldColor = Colors.black;
    charts.Color newColor = toChartColor(oldColor);
    expect(newColor, TypeMatcher<charts.Color>());
    expect(newColor.r, oldColor.red);
    expect(newColor.g, oldColor.green);
    expect(newColor.b, oldColor.blue);
    expect(newColor.a, oldColor.alpha);
  });
}
