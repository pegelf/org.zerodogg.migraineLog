// Migraine Log - a simple multi-platform headache diary
// Copyright (C) 2021    Eskild Hustvedt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

import 'package:matcher/matcher.dart';
import 'package:MigraineLog/localehack.dart';
import 'package:flutter/material.dart' as flutter_mat;
import 'package:flutter_test/flutter_test.dart';
import 'package:intl/intl.dart';

void main() {
  setUp(() async {
    LocaleHack.reset();
  });
  group('getFlutterLocale', () {
    testWidgets('en', (WidgetTester tester) async {
      tester.binding.window.localesTestValue = [flutter_mat.Locale('en')];
      tester.binding.window.localeTestValue = flutter_mat.Locale('en');
      expect(LocaleHack.getFlutterLocale(), TypeMatcher<flutter_mat.Locale>());
      expect(LocaleHack.getFlutterLocale().toString(), 'en');
    });
    testWidgets('nn', (WidgetTester tester) async {
      tester.binding.window.localesTestValue = [flutter_mat.Locale('nn')];
      tester.binding.window.localeTestValue = flutter_mat.Locale('nn');
      expect(LocaleHack.getFlutterLocale(), TypeMatcher<flutter_mat.Locale>());
      expect(LocaleHack.getFlutterLocale().toString(), 'nb');
    });
    testWidgets('nb', (WidgetTester tester) async {
      tester.binding.window.localesTestValue = [flutter_mat.Locale('nb')];
      tester.binding.window.localeTestValue = flutter_mat.Locale('nb');
      expect(LocaleHack.getFlutterLocale(), TypeMatcher<flutter_mat.Locale>());
      expect(LocaleHack.getFlutterLocale().toString(), 'nb');
    });
  });
  group('detectLocale', () {
    testWidgets('en', (WidgetTester tester) async {
      tester.binding.window.localesTestValue = [flutter_mat.Locale('en')];
      tester.binding.window.localeTestValue = flutter_mat.Locale('en');
      expect(LocaleHack.detectLocale(), TypeMatcher<String>());
      expect(LocaleHack.detectLocale(), 'en');
    });
    testWidgets('nn', (WidgetTester tester) async {
      tester.binding.window.localesTestValue = [flutter_mat.Locale('nn')];
      tester.binding.window.localeTestValue = flutter_mat.Locale('nn');
      expect(LocaleHack.detectLocale(), TypeMatcher<String>());
      expect(LocaleHack.detectLocale(), 'nn');
    });
    testWidgets('nb', (WidgetTester tester) async {
      tester.binding.window.localesTestValue = [flutter_mat.Locale('nb')];
      tester.binding.window.localeTestValue = flutter_mat.Locale('nb');
      expect(LocaleHack.detectLocale(), TypeMatcher<String>());
      expect(LocaleHack.detectLocale(), 'nn');
    });
  });
  testWidgets('intlInit', (WidgetTester tester) async {
    expect(Intl.defaultLocale, isNull);
    LocaleHack.intlInit();
    expect(Intl.defaultLocale, LocaleHack.detectLocale());
  });
  group('dateLocale', () {
    testWidgets('en', (WidgetTester tester) async {
      tester.binding.window.localesTestValue = [flutter_mat.Locale('en')];
      tester.binding.window.localeTestValue = flutter_mat.Locale('en');
      expect(LocaleHack.dateLocale, TypeMatcher<String>());
      expect(LocaleHack.dateLocale, 'en');
    });
    testWidgets('nn', (WidgetTester tester) async {
      tester.binding.window.localesTestValue = [flutter_mat.Locale('nn')];
      tester.binding.window.localeTestValue = flutter_mat.Locale('nn');
      expect(LocaleHack.dateLocale, TypeMatcher<String>());
      expect(LocaleHack.dateLocale, 'no_NO');
    });
    testWidgets('nb', (WidgetTester tester) async {
      tester.binding.window.localesTestValue = [flutter_mat.Locale('nb')];
      tester.binding.window.localeTestValue = flutter_mat.Locale('nb');
      expect(LocaleHack.dateLocale, TypeMatcher<String>());
      expect(LocaleHack.dateLocale, 'no_NO');
    });
  });
}
